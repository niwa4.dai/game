using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributeDisplay : MonoBehaviour
{
    /*[SerializeField]
    private ActorAttribute attribute;*/

    [SerializeField]
    private Text attributeName, attributeValue, attributeModif;

     public void Init(ActorAttribute _Atribute, int _Value)
    {
        attributeName.text = _Atribute.ToString();
        attributeValue.text = _Value.ToString();
        attributeModif.text = "";
    }

    /*public void Init(CharacterDataObject _CharacterData)
    {
        attributeName.text = attribute.ToString();
        attributeValue.text = _CharacterData.attributes.GetAttribute(attribute).ToString();
        attributeModif.text = "";
    }*/
}
