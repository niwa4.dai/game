using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortraitDisplay : MonoBehaviour
{
    [SerializeField]
    private Image image;

    private MenuBehaviour menu;
    private CharacterDataObject characterData;

    public void Init(MenuBehaviour _Menu, CharacterDataObject _CharacterDataObject)
    {
        menu = _Menu;
        characterData = _CharacterDataObject;

        image.sprite = characterData.portrait;
    }

    public void OnClick()
    {
        menu.SelectCharacter(characterData);
    }
}
