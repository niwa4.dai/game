using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputInterfaceDisplay : MonoBehaviour
{
    [SerializeField]
    private Text inputActionText;
    [SerializeField]
    private Text inputKeyText;

    public void Display(string _Action, KeyCode _Key)
    {
        inputActionText.text = _Action;
        inputKeyText.text = _Key.ToString();
    }
}
