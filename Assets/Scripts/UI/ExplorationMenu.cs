using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplorationMenu : MenuBehaviour
{
    [SerializeField]
    private RectTransform portraitsContainer;
    [SerializeField]
    private PortraitDisplay portraitPrefab;

    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private CharacterMenu characterMenu;


    public void Open()
    {
        Toggle(true);
        mainMenu.SetActive(true);
        characterMenu.Close();

        List<PortraitDisplay> portraitDisplays = Utils.PopulateList(portraitPrefab, PlayerController.Instance.playerGroup.Count, portraitsContainer);

        for (int i = 0; i < portraitDisplays.Count; i++)
        {
            portraitDisplays[i].Init(this, PlayerController.Instance.playerGroup[i].data);
        }
    }

    public override void Close()
    {
        UIManager.Instance.ToggleHUD(true);
        TimeManager.Play();

        base.Close();
    }

    public override void SelectCharacter(CharacterDataObject _CharacterData)
    {
        base.SelectCharacter(_CharacterData);

        mainMenu.SetActive(false);
        characterMenu.Open(_CharacterData);
    }
}
