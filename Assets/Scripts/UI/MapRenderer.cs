using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MapRenderer : Singleton<MapRenderer>
{
    [SerializeField]
    private Camera mapCamera;
    [SerializeField]
    private float maxCamHeight = 20f;
    [SerializeField]
    private float minCamHeight = 1.5f;
    [SerializeField]
    private float farPlane = 20f;
    [SerializeField]
    private float scrollSpeed = 2f;
    [SerializeField]
    private float zoomSpeed = 2f;

    [SerializeField]
    private MeshFilter navMeshFilter;

    private Mesh navMeshTriangulate = null;

    private Dictionary<ExplorationAreaObject, List<MapCamera>> cameras = new Dictionary<ExplorationAreaObject, List<MapCamera>>();
    private MapCamera currentMapCamera;
    private float orthographicSize = 5f;

    protected override void Awake()
    {
        base.Awake();
    }

    public void ToggleMap(bool _Value)
    {
        if (_Value == false) 
        {
            if (currentMapCamera.camera != null)
                currentMapCamera.camera.gameObject.SetActive(_Value);

            return;
        }

        if (cameras.TryGetValue(ExplorationAreaBehaviour.currentExplorationArea, out List<MapCamera> mapCameras) == false)
            return;

        Vector3 playerPos = PlayerController.Instance.characterControlled.transform.position;
        currentMapCamera = new MapCamera();
        float closestDistance = Mathf.Infinity;

        foreach(MapCamera mapCamera in mapCameras)
        {
            float distance = (mapCamera.camera.transform.position.y - 0.5f) -playerPos.y;

            if (distance <= 0)
                continue;

            if (currentMapCamera.camera == null || distance < closestDistance)
            {
                currentMapCamera = mapCamera;
                closestDistance = distance;
            }
        }

        if (currentMapCamera.camera == null)
            return;

        currentMapCamera.camera.gameObject.SetActive(_Value);

        /*if(navMeshTriangulate == null)
        {
            NavMeshTriangulation triangulation = NavMesh.CalculateTriangulation();

            navMeshTriangulate = new Mesh();
            navMeshTriangulate.vertices = triangulation.vertices;
            navMeshTriangulate.triangles = triangulation.indices;
            navMeshFilter.mesh = navMeshTriangulate;
        }*/

        Vector3 newCamPos = playerPos;
        newCamPos.y = currentMapCamera.camera.transform.position.y;
        currentMapCamera.camera.transform.position = newCamPos;
        
        currentMapCamera.camera.orthographicSize = orthographicSize;

        ClampCameraOrthographicSize(currentMapCamera.camera, currentMapCamera.limits);
        ClampCameraPosition(currentMapCamera.camera, currentMapCamera.limits);

        UIManager.Instance.UpdateMapUI(currentMapCamera.camera);
    }

    public void ScrollMap(Vector2 _Delta)
    {
        if (currentMapCamera.camera == null)
            return;

        currentMapCamera.camera.transform.Translate(new Vector3(_Delta.x, 0, _Delta.y) * scrollSpeed * orthographicSize * Time.unscaledDeltaTime, Space.World);
        ClampCameraPosition(currentMapCamera.camera, currentMapCamera.limits);
        UIManager.Instance.UpdateMapUI(currentMapCamera.camera);
    }

    public void ZoomMap(float _Delta)
    {
        if (currentMapCamera.camera == null)
            return;

        currentMapCamera.camera.orthographicSize = orthographicSize - _Delta * zoomSpeed * Time.unscaledTime;
        
        ClampCameraOrthographicSize(currentMapCamera.camera, currentMapCamera.limits);
        ClampCameraPosition(currentMapCamera.camera, currentMapCamera.limits);
        UIManager.Instance.UpdateMapUI(currentMapCamera.camera);
    }

    public void ExpandMap(ExplorationAreaObject _ExplorationArea, MapCamera _MapCamera)
    {
        if (cameras.ContainsKey(_ExplorationArea) == true)
            cameras[_ExplorationArea].Add(_MapCamera);
        else
            cameras.Add(_ExplorationArea, new List<MapCamera>() { _MapCamera });
    }

    private void ClampCameraOrthographicSize(Camera _Camera, Vector4 _Limits)
    {
        orthographicSize = _Camera.orthographicSize;
        orthographicSize = Mathf.Clamp(orthographicSize, 1f, (currentMapCamera.limits.w - currentMapCamera.limits.z) * 0.5f);
        orthographicSize = Mathf.Clamp(orthographicSize, 1f, (currentMapCamera.limits.y - currentMapCamera.limits.x) * 0.5f / currentMapCamera.camera.aspect);

        currentMapCamera.camera.orthographicSize = orthographicSize;
    }

    private void ClampCameraPosition (Camera _Camera, Vector4 _Limits)
    {
        Vector3 camPos = _Camera.transform.position;

        float halfHeight = _Camera.orthographicSize;
        float halfWidth = halfHeight * _Camera.aspect;

        camPos.z = Mathf.Clamp(camPos.z, _Limits.z + halfHeight, _Limits.w - halfHeight);
        camPos.x = Mathf.Clamp(camPos.x, _Limits.x + halfWidth, _Limits.y - halfWidth);

        _Camera.transform.position = camPos;
    }
}

public struct MapCamera
{
    public Camera camera;
    public Vector4 limits;

    public MapCamera(Camera camera, Vector4 limits)
    {
        this.camera = camera;
        this.limits = limits;
    }
}