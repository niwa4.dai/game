using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public bool mapOpen { get; private set; }

    [SerializeField]
    private ExplorationMenu explorationMenu;

    [SerializeField]
    private GameObject hud;
    [SerializeField]
    private GameObject mapUI;
    [SerializeField]
    private RectTransform playerMapIcon;
    [SerializeField]
    private Text cinematicSkipText;
    [SerializeField]
    private Animation explorationAreaPopUp;
    [SerializeField]
    private Text explorationAreaText;
    [SerializeField]
    private RectTransform compassCardinals;

    [Header("Dialogues")]

    [SerializeField]
    private GameObject dialogueBox;
    [SerializeField]
    private Text dialogueNameText, dialogueSentenceText;
    private Coroutine sentenceTyping;

    private const KeyCode dialogueKey = KeyCode.E;

    [Header("Reduced Dialogues")]

    [SerializeField]
    private RectTransform reducedDialogueBoxPrefab;

    private Dictionary<CharacterDataObject, RectTransform> speakersBoxes = new Dictionary<CharacterDataObject, RectTransform>();

    [Header("Intputs display")]

    [SerializeField]
    private InputInterfaceDisplay interactionDisplay;
    [SerializeField]
    private InputInterfaceDisplay switchCharacterDisplay, openMapDisplay, closeMapDisplay, openMenuDisplay;

    private Vector2 oldMousePos;

    protected override void Awake()
    {
        base.Awake();

        HideDialogue();

        switchCharacterDisplay.Display("Switch character", PlayerController.switchCharacterKey);
        openMapDisplay.Display("Open map", PlayerController.mapKey);
        closeMapDisplay.Display("Close map", PlayerController.mapKey);

        openMenuDisplay.Display("Open menu", PlayerController.menuKey);

        cinematicSkipText.text = string.Format("Press {0} to skip", PlayableDirectorSkipper.skipKey.ToString());


        HideActionPrompt();
        HideExplorationAreaPopUp();
        ToggleMapUI(false);
    }

    private void Update()
    {
        if (mapOpen == false)
        {
            if (Input.GetKeyDown(dialogueKey))
            {
                if (sentenceTyping != null)
                    DialogueManager.DisplayCurrentSentence(false);
                else
                    DialogueManager.DisplayNextSentence();
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                oldMousePos = Input.mousePosition;
            }

            if (Input.GetMouseButton(0))
            {
                Vector2 newMousePos = Input.mousePosition;
                Vector2 delta = oldMousePos - newMousePos;

                MapRenderer.Instance.ScrollMap(delta);

                oldMousePos = newMousePos;
            }

            float zoom = Input.GetAxis("Mouse ScrollWheel");

            if (zoom != 0)
            {
                MapRenderer.Instance.ZoomMap(zoom);
            }
        }
    }

    public void OpenMenu()
    {
        TimeManager.Pause();

        ToggleHUD(false);
        explorationMenu.Open();
    }


    public void ToggleHUD(bool _Value)
    {
        hud.gameObject.SetActive(_Value);
    }

    public void ToggleMapUI(bool _Value)
    {
        if (_Value == true)
            TimeManager.Pause();
        else
            TimeManager.Play();

        ToggleHUD(!_Value);
        mapUI.gameObject.SetActive(_Value);
        mapOpen = _Value;

        if (MapRenderer.Instance != null)
            MapRenderer.Instance.ToggleMap(_Value);
    }

    public void UpdateMapUI(Camera _MapCamera)
    {
        playerMapIcon.anchoredPosition = _MapCamera.WorldToScreenPoint(PlayerController.Instance.characterControlled.transform.position);
        playerMapIcon.localEulerAngles = Vector3.forward * -PlayerController.Instance.characterControlled.transform.eulerAngles.y;
    }

    public void DisplayActionPrompt(string _Action, KeyCode _Key)
    {
        interactionDisplay.gameObject.SetActive(true);
        interactionDisplay.Display(_Action, _Key);
    }

    public void HideActionPrompt()
    {
        interactionDisplay.gameObject.SetActive(false);
    }

    public void DisplayExplorationAreaPopUp(string _Name)
    {
        explorationAreaPopUp.gameObject.SetActive(true);
        explorationAreaPopUp.Play();
        explorationAreaText.text = _Name;

        StartCoroutine(Utils.DelayedAction(5f, () => HideExplorationAreaPopUp()));
    }

    public void HideExplorationAreaPopUp()
    {
        explorationAreaPopUp.gameObject.SetActive(false);
    }

    //Must display full sentence before next if sentenceTyping != null !
    public void DisplayDialogue(Sentence _Sentence, bool _TypingCoroutine)
    {
        dialogueBox.SetActive(true);
        dialogueNameText.text = _Sentence.speaker.firstName;

        if (sentenceTyping != null)
        {
            StopCoroutine(sentenceTyping);
            sentenceTyping = null;
        }

        if (_TypingCoroutine == true)
            sentenceTyping = StartCoroutine(TypeSentence(dialogueSentenceText, _Sentence.text, () => sentenceTyping = null));
        else
            dialogueSentenceText.text = _Sentence.text;
    }

    public void HideDialogue()
    {
        dialogueBox.SetActive(false);
    }

    public void DisplayReducedDialogue(Dialogue _Dialogue)
    {
        Sentence sentence = _Dialogue.GetNextSentence();

        if (sentence == null)
            return;

        if (speakersBoxes.ContainsKey(sentence.speaker))
            return;

        RectTransform newBox = Instantiate(reducedDialogueBoxPrefab, transform);
        Text newText = newBox.GetComponentInChildren<Text>();

        speakersBoxes.Add(sentence.speaker, newBox);

        StartCoroutine(PlaceReducedDialogueBox(speakersBoxes[sentence.speaker], sentence.speaker));
        StartCoroutine(TypeSentence(newText, sentence.text, () =>
        {
            StartCoroutine(Utils.DelayedAction(4f, () =>
            {
                Destroy(newBox.gameObject);
                speakersBoxes.Remove(sentence.speaker);
                DisplayReducedDialogue(_Dialogue);
            }));
        }));
    }

    public void UpdateCompass(float _RotationY)
    {
        float posX = (compassCardinals.sizeDelta.x / 5) * 4 * 0.5f;
        Vector2 posStart = new Vector2(posX, 0);
        Vector2 posEnd = new Vector2(-posX, 0);

        compassCardinals.anchoredPosition = Vector2.Lerp(posStart, posEnd, _RotationY / 360);
    }

    // Type sentence, letter per letter
    private IEnumerator TypeSentence(Text _Text, string _Sentence, System.Action _Action)
    {
        _Text.text = "";

        //Must add Time.deltaTime !
        foreach (char letter in _Sentence)
        {
            _Text.text += letter;
            yield return null;
        }

        if (_Action != null)
            _Action();
    }

    private IEnumerator PlaceReducedDialogueBox(RectTransform _Box, CharacterDataObject _SpeakerData)
    {
        CharacterBehaviour speaker = CharacterReferencesManager.GetReference(_SpeakerData);
        Camera camera = Camera.main;

        while (_Box != null)
        {
            Vector3 direction = speaker.transform.position - camera.transform.position;
            if (Vector3.Dot(direction, camera.transform.forward) < 0 || direction.magnitude > 10)
                _Box.gameObject.SetActive(false);
            else
                _Box.gameObject.SetActive(true);

            Vector3 pos = camera.WorldToScreenPoint(speaker.transform.position + Vector3.up * 1.8f);
            //_Box.anchoredPosition = pos;
            _Box.position = pos;

            yield return null;
        }
    }
}
