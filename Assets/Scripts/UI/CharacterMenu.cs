using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMenu : MenuBehaviour
{
    [Header("Character Infos")]

    [SerializeField]
    private PortraitDisplay characterPortrait;
    [SerializeField]
    private Text characterName;
    [SerializeField]
    private RectTransform classesContainer;
    [SerializeField]
    private ClassDisplay classDisplayPrefab;

    [Header("Attributes")]

    [SerializeField]
    private RectTransform attributesContainer;
    [SerializeField]
    private AttributeDisplay attributeDisplayPrefab;

    public void Open(CharacterDataObject _CharacterData)
    {
        Toggle(true);

        characterPortrait.Init(this, _CharacterData);
        characterName.text = _CharacterData.firstName + " " + _CharacterData.lastName;

        List<ClassDisplay> classDisplays = Utils.PopulateList(classDisplayPrefab, _CharacterData.classes.Length, classesContainer);

        for (int i = 0; i <_CharacterData.classes.Length; i++)
        {
            classDisplays[i].Init(_CharacterData.classes[i]);
        }

        for (int i = 0; i < System.Enum.GetValues(typeof(ActorAttribute)).Length; i++)
        {
            ActorAttribute attribute = (ActorAttribute)i;

            if (attribute.IsVisible() == false)
                continue;

            AttributeDisplay attributeDisplay;

            if (i >= attributesContainer.childCount)
                attributeDisplay = Instantiate(attributeDisplayPrefab, attributesContainer);
            else
                attributeDisplay = attributesContainer.GetChild(i).GetComponent<AttributeDisplay>();


            attributeDisplay.Init(attribute, _CharacterData.attributes.GetAttribute(attribute));
        }

        /*foreach (AttributeDisplay attributeDisplay in attributesContainer.GetComponentsInChildren< AttributeDisplay>())
        {
            attributeDisplay.Init(_CharacterData);
        }*/
    }
}
