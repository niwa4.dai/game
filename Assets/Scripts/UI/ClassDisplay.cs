using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassDisplay : MonoBehaviour
{
    [SerializeField]
    private Image image;

    private CharacterClassObject characterClass;

    public void Init(CharacterClassObject _CharacterClassObject)
    {
        characterClass = _CharacterClassObject;
        image.sprite = characterClass.icon;
    }
}
