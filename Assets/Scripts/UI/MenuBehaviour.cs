using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MenuBehaviour : MonoBehaviour
{
    public virtual void Close() 
    {
        Toggle(false);
    }
    public virtual void SelectCharacter(CharacterDataObject _CharacterData) { }

    protected void Toggle(bool _Value)
    {
        gameObject.SetActive(_Value);
    }
}
