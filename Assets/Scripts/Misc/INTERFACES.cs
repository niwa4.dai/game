using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public interface ISaveable
{
    public void Save(BinaryFormatter _BF, FileStream _File);
    public void Load(BinaryFormatter _BF, FileStream _File);
}

public interface IInteractable
{
    public void Activate(CharacterBehaviour _Activator);
    public void SetAsInteractableInRange(CharacterBehaviour _Character);
    public void RemoveFromInteractableInRange(CharacterBehaviour _Character);
}