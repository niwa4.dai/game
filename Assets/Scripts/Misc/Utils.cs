using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static IEnumerator DelayedAction(float _Delay, System.Action _Action)
    {
        yield return new WaitForSeconds(_Delay);

        _Action();
    }

    public static List<T> PopulateList<T>(T prefab, int count, Transform parent) where T : Component
    {
        List<T> output = new List<T>();

        T[] instantiatedPrefabs = parent.GetComponentsInChildren<T>(true);

        foreach (T instantiatedPrefab in instantiatedPrefabs)
        {
            if (output.Count < count)
            {
                instantiatedPrefab.gameObject.SetActive(true);
                output.Add(instantiatedPrefab);
            }
            else instantiatedPrefab.gameObject.SetActive(false);
        }

        while (output.Count < count)
        {
            T newPrefab = Object.Instantiate(prefab, parent) as T;
            newPrefab.gameObject.SetActive(true);
            output.Add(newPrefab);
        }

        return output;
    }
}

[System.Serializable]
public struct MinMax
{
    public float min;
    public float max;

    public MinMax(float min, float max)
    {
        this.min = min;
        this.max = max;
    }
}

[System.Serializable]
public struct MinMaxInt
{
    public int min;
    public int max;

    public MinMaxInt(int min, int max)
    {
        this.min = min;
        this.max = max;
    }
}