using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterAbility
{
    None, Acrobatic, Erudition, Heavyweight
}

public enum ActorAttribute
{
    Health, MoveSpeed, AttackSpeed, Strength, Defense, Accuracy, Magic, Spirit, Luck, CrystalCapacity
}

public enum AIBehaviour
{
    Follow, Patrol
}

public enum Comparator
{
    Greater, Less, Equals, NotEqual
}