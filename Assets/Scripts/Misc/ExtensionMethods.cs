using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static bool IsVisible(this ActorAttribute _ActorAttribute)
    {
        switch (_ActorAttribute)
        {
            case ActorAttribute.Health:
                return true;
            case ActorAttribute.Strength:
                return true;
            case ActorAttribute.Defense:
                return true;
            case ActorAttribute.Magic:
                return true;
            case ActorAttribute.Spirit:
                return true;
            case ActorAttribute.Luck:
                return true;
            default:
                return false;
        }
    }
}
