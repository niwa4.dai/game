using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Playables;

public class PlayableDirectorSkipper : MonoBehaviour, ISaveable
{
    public const KeyCode skipKey = KeyCode.Space;
    private PlayableDirector playableDirector;

    private bool played;

    private void Awake()
    {
        playableDirector = GetComponent<PlayableDirector>();
        playableDirector.stopped += (PlayableDirector d) => { played = true; };
    }

    void Update()
    {
        if (playableDirector.state != PlayState.Playing)
            return;

        if (Input.GetKeyDown(skipKey))
        {
            playableDirector.time = playableDirector.playableAsset.duration;    // set the time to the last frame
            playableDirector.Evaluate();                                        // evaluates the timeline
        }
    }

    #region Save

    [System.Serializable]
    public class SavedData
    {
        public double time;

        public SavedData(double time)
        {
            this.time = time;
        }
    }

    public void Save(BinaryFormatter _BF, FileStream _File)
    {
        double time = 0;

        if (played == true)
            time = playableDirector.playableAsset.duration;
        else if (playableDirector.state == PlayState.Playing)
            time = playableDirector.time;

        SavedData data = new SavedData(time);

        _BF.Serialize(_File, data);
    }

    public void Load(BinaryFormatter _BF, FileStream _File)
    {
        SavedData data = (SavedData)_BF.Deserialize(_File);

        if (data == null)
        {
            Debug.Log("No data");
        }

        playableDirector.playOnAwake = false;
        playableDirector.time = data.time;
        playableDirector.Evaluate();

        if (playableDirector.time <= 0)
            playableDirector.Stop();
        else
            playableDirector.Play();
    }

    #endregion
}
