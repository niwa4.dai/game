using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class FlickeringLight : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Minimum random light intensity factor")]
    private float minIntensity = 0.5f;
    [SerializeField]
    [Tooltip("Maximum random light intensity factor")]
    private float maxIntensity = 1.5f;
    [SerializeField]
    [Tooltip("How much to smooth out the randomness; lower values = sparks, higher = lantern")]
    [Range(1, 50)]
    private int smoothing = 5;

    // Continuous average calculation via FIFO queue
    // Saves us iterating every time we update, we just change by the delta
    private Queue<float> smoothQueue;
    private float lastSum = 0;

    new private Light light;
    private float defaultIntensity;

    private void Awake()
    {
        light = GetComponent<Light>();

        if (light == null)
            return;

        defaultIntensity = light.intensity;
        smoothQueue = new Queue<float>(smoothing);
    }

    private void Update()
    {
        if (light == null)
            return;

        // Pop off an item if too big
        while (smoothQueue.Count >= smoothing)
        {
            lastSum -= smoothQueue.Dequeue();
        }

        // Generate random new item, calculate new average
        float newVal = Random.Range(minIntensity * defaultIntensity, maxIntensity * defaultIntensity);
        smoothQueue.Enqueue(newVal);
        lastSum += newVal;

        // Calculate new smoothed average
        light.intensity = lastSum / (float)smoothQueue.Count;
    }
}
