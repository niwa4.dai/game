using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AttributesStruct
{
    [SerializeField]
    private int[] attributesValues;
    private Dictionary<ActorAttribute, int> attributes;


    public int GetAttribute(ActorAttribute _Attribute)
    {
        if (attributes == null)
        {
            attributes = new Dictionary<ActorAttribute, int>();

            foreach (int i in System.Enum.GetValues(typeof(ActorAttribute)))
            {
                attributes.Add((ActorAttribute)i, attributesValues[i]);
            }
        }

        return attributes[_Attribute];
    }
}
