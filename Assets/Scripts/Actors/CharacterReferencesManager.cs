using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CharacterReferencesManager
{
    private static Dictionary<CharacterDataObject, CharacterBehaviour> characterReferences = new Dictionary<CharacterDataObject, CharacterBehaviour>();

    public static void AddReference(CharacterDataObject _Data, CharacterBehaviour _Reference)
    {
        //To change references between scenes
        if (characterReferences.ContainsKey(_Data))
        {
            Debug.Log(characterReferences[_Data] + " overrided by " + _Reference);
            characterReferences[_Data] = _Reference;
        }
        else
            characterReferences.Add(_Data, _Reference);
    }

    public static CharacterBehaviour GetReference(CharacterDataObject _Data)
    {
        characterReferences.TryGetValue(_Data, out CharacterBehaviour output);

        return output;
    }
}
