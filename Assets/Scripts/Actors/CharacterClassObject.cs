using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewClass", menuName = "Scriptable Objects/Actors/Class")]
public class CharacterClassObject : ScriptableObject
{
    public Sprite icon;
}
