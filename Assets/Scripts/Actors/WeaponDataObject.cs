using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWeapon", menuName = "Scriptable Objects/Items/Weapon")]
public class WeaponDataObject : ItemDataObject
{
    public GameObject model;
}