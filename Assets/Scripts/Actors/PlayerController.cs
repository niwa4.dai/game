using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : Singleton<PlayerController>
{
    public CharacterBehaviour characterControlled { get; private set; }
    public UnityAction<CharacterBehaviour> onCharacterChangeAction;

    [SerializeField]
    public List<CharacterBehaviour> playerGroup = new List<CharacterBehaviour>();
    [SerializeField]
    private CinemachineVirtualCamera virtualCamera;
    [SerializeField]
    private float sensibility = 1f;
    [SerializeField]
    private bool inverseVertical;

    private const string interactionText = "Interact";

    private const KeyCode interactionKey = KeyCode.E;
    public const KeyCode switchCharacterKey = KeyCode.T;
    public const KeyCode mapKey = KeyCode.M;
    public const KeyCode menuKey = KeyCode.Escape;

    private const float cameraAgularSpeed = 40f;

    private Quaternion cameraRotation;

    private bool controlsLocked = false;

    private int stepCount;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        if (playerGroup.Count == 0)
            return;

        RefreshPlayableCharacter();
    }

    private void Update()
    {
        if (characterControlled == null)
            return;

        if (controlsLocked == true)
            return;

        if (Input.GetKeyDown(menuKey) == true)
            UIManager.Instance.OpenMenu();

        if (Input.GetKeyDown(mapKey) == true)
            UIManager.Instance.ToggleMapUI(!UIManager.Instance.mapOpen);

        if (UIManager.Instance.mapOpen == true)
            return;

        CameraControls();

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 delta = new Vector3(horizontal, 0, vertical);
        characterControlled.Move(delta);

        if (characterControlled.affectedByCinematic == true)
            return;

        if (Input.GetKeyDown(interactionKey) && characterControlled.interactableInRange != null)
            characterControlled.interactableInRange.Activate(characterControlled);

        if (Input.GetKeyDown(switchCharacterKey))
        {
            playerGroup.Remove(characterControlled);
            playerGroup.Add(characterControlled);

            RefreshPlayableCharacter();
        }
    }

    private void CameraControls()
    {
        characterControlled.cameraTarget.rotation = cameraRotation;

        float cameraHorizontal = Input.GetAxis("Mouse X");
        float cameraVertical = Input.GetAxis("Mouse Y");

        Vector2 cameraDelta = new Vector2(cameraHorizontal, cameraVertical);

        if (inverseVertical == true)
            cameraDelta.y = -cameraDelta.y;

        cameraDelta *= cameraAgularSpeed * sensibility * Time.deltaTime;

        characterControlled.cameraTarget.rotation *= Quaternion.AngleAxis(cameraDelta.x, Vector3.up);
        characterControlled.cameraTarget.rotation *= Quaternion.AngleAxis(cameraDelta.y, Vector3.right);

        Vector3 angles = characterControlled.cameraTarget.localEulerAngles;
        angles.z = 0;

        if (angles.x > 180 && angles.x < 340)
        {
            angles.x = 340;
        }
        else if (angles.x < 180 && angles.x > 40)
        {
            angles.x = 40;
        }
        characterControlled.cameraTarget.localEulerAngles = angles;

        cameraRotation = characterControlled.cameraTarget.rotation;

        UIManager.Instance.UpdateCompass(characterControlled.cameraTarget.eulerAngles.y);
    }

    public void RefreshPlayableCharacter() 
    { 
        SetPlayableCharacter(playerGroup[0]); 
    }

    private void SetPlayableCharacter(CharacterBehaviour _Character)
    {
        if (characterControlled != null)
        {
            characterControlled.tag = "Untagged";
            characterControlled.onInteractableChange = null;
            characterControlled.onStep = null;
        }

        characterControlled = _Character;
        characterControlled.tag = "Player";
        characterControlled.aiController.isActive = false;

        cameraRotation = characterControlled.transform.rotation;
        virtualCamera.Follow = characterControlled.cameraTarget;

        characterControlled.onInteractableChange += (IInteractable interactable) =>
        {
            if (interactable == null)
                UIManager.Instance.HideActionPrompt();
            else
                UIManager.Instance.DisplayActionPrompt(interactionText, interactionKey);
        };
        characterControlled.onStep += () => 
        { 
            stepCount++;
            EncounterManager.stepsBeforeEncounter--;
        };

        onCharacterChangeAction.Invoke(characterControlled);
        characterControlled.onInteractableChange.Invoke(characterControlled.interactableInRange);


        UpdateGroupAI();
    }

    private void UpdateGroupAI()
    {
        for (int i = 1; i < playerGroup.Count; i++)
        {
            //playerGroup[i].Follow(playerGroup[i - 1]);
            playerGroup[i].Follow(characterControlled, AIController.followDistance * i);
        }
    }

    public void TogglePlayerControls(bool _Value)
    {
        characterControlled.Move(Vector3.zero);
        controlsLocked = !_Value;
    }


}