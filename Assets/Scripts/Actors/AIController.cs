using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class AIController
{
    public bool isActive
    {
        get { return _isActive; }
        set
        {
            if (value == false && stopAllCoroutines != null)
            {
                stopAllCoroutines.Invoke();
                stopAllCoroutines = null;
            }
            _isActive = value;
        }
    }
    private bool _isActive = true;

    [SerializeField]
    private List<AIPackage> packages = new List<AIPackage>();
    private AIPackage currentPackage;

    private UnityAction stopAllCoroutines;

    public const float followDistance = 2f;

    public void EvaluateCurrentPackage(CharacterBehaviour _Character)
    {
        if (packages.Count == 0)
            return;

        //If currentPackage is complete and no packages are valid
        AIPackage oldPackage = currentPackage;

        foreach (AIPackage package in packages)
        {
            if (package.condition.valid == true)
            {
                if (currentPackage == package)
                    return;
                else if (package.path.Length > 0)
                {
                    currentPackage = package;
                    break;
                }
            }

        }

        if (currentPackage == oldPackage)
            return;

        /*AIPackage topPackage = packages[0];

        if (topPackage == currentPackage)
            return;

        currentPackage = topPackage;

        if (currentPackage.path.Length == 0)
            return;*/

        if (stopAllCoroutines != null)
            stopAllCoroutines.Invoke();

        stopAllCoroutines = null;
        Coroutine coroutine = null;

        if (currentPackage.behaviour == AIBehaviour.Follow)
        {
            coroutine = _Character.StartCoroutine(FollowCoroutine(_Character));
        }
        else if (currentPackage.behaviour == AIBehaviour.Patrol)
        {
            coroutine = _Character.StartCoroutine(PatrolCoroutine(_Character));
        }

        stopAllCoroutines += () => { if (coroutine != null) _Character.StopCoroutine(coroutine); };
    }

    public void AddPackage(AIPackage _Package)
    {
        packages.Add(_Package);
    }

    public void SetPackages(CharacterBehaviour _Character, List<AIPackage> _Packages)
    {
        ClearPackages();
        packages = _Packages;
        EvaluateCurrentPackage(_Character);
    }

    public List<AIPackage> GetPackages()
    {
        return packages;
    }

    public void ClearPackages()
    {
        packages.Clear();
    }

    private void CompleteCurrentPackage()
    {
        if (currentPackage.completionConsequence.quest != null)
            currentPackage.completionConsequence.UpdateQuest();

        packages.RemoveAt(0);

        if (currentPackage.deleteOnCompletion == false)
        {
            packages.Add(currentPackage);
        }

        if (stopAllCoroutines != null)
            stopAllCoroutines.Invoke();

        stopAllCoroutines = null;
    }

    private IEnumerator FollowCoroutine(CharacterBehaviour _Character)
    {
        _Character.agent.stoppingDistance = currentPackage.stoppingDistance;
        float completionTime = (currentPackage.autoComplete == true) ? currentPackage.completionTime : Mathf.Infinity;
        _Character.agent.speed = CharacterBehaviour.runningSpeed;

        for (float f = 0; f < completionTime; f += Time.deltaTime)
        {
            _Character.agent.SetDestination(currentPackage.path[0].transform.position);

            /*if(_Character.agent.remainingDistance > 5f)
                _Character.agent.speed = CharacterBehaviour.runningSpeed;
            else
                _Character.agent.speed = CharacterBehaviour.walkingSpeed;*/

            yield return null;
        }

        CompleteCurrentPackage();
    }

    private IEnumerator PatrolCoroutine(CharacterBehaviour _Character)
    {
        _Character.agent.stoppingDistance = currentPackage.stoppingDistance;
        _Character.agent.speed = (currentPackage.canRun) ? CharacterBehaviour.runningSpeed : CharacterBehaviour.walkingSpeed;

        int pathIndex = 0;

        _Character.agent.SetDestination(currentPackage.path[pathIndex].transform.position);

        while (_Character.gameObject.activeInHierarchy == true)
        {
            yield return null;

            if (_Character.agent.isStopped == true)
                continue;

            // Check if we've reached the destination
            if (!_Character.agent.pathPending)
            {
                if (_Character.agent.remainingDistance <= _Character.agent.stoppingDistance)
                {
                    if (!_Character.agent.hasPath || _Character.agent.velocity.sqrMagnitude == 0f)
                    {
                        //Add a "LookAt" function to face destination

                        for (float f = 0; f < currentPackage.waitingTimeOnPath; f += Time.deltaTime)
                        {
                            yield return null;
                        }

                        pathIndex++;

                        if (pathIndex >= currentPackage.path.Length)
                        {
                            if (currentPackage.autoComplete == true)
                            {
                                CompleteCurrentPackage();
                                /*Coroutine coroutine = _Character.StartCoroutine(Utils.DelayedAction(currentPackage.completionTime, () =>
                                {
                                    CompleteCurrentPackage();
                                }));
                                stopAllCoroutines += () => { if (coroutine != null) _Character.StopCoroutine(coroutine); };*/

                                break;
                            }
                            else
                            {
                                pathIndex = 0;
                            }
                        }

                        _Character.agent.SetDestination(currentPackage.path[pathIndex].transform.position);
                    }
                }
            }
        }
    }
}

[System.Serializable]
public class AIPackage
{
    public QuestCondition condition;
    public AIBehaviour behaviour;
    public GameObject[] path = new GameObject[0];

    public float waitingTimeOnPath;

    public float stoppingDistance;
    public bool canRun;

    public bool autoComplete;
    public float completionTime;
    public bool deleteOnCompletion;

    public QuestUpdate completionConsequence;

    public AIPackage(AIBehaviour behaviour, GameObject[] path, float waitingTimeOnPath, float stoppingDistance, bool canRun)
    {
        this.behaviour = behaviour;
        this.path = path;
        this.waitingTimeOnPath = waitingTimeOnPath;
        this.stoppingDistance = stoppingDistance;
        this.canRun = canRun;

        autoComplete = false;
        completionTime = 0f;
        deleteOnCompletion = false;
    }

    public AIPackage(AIBehaviour behaviour, GameObject[] path, float waitingTimeOnPath, float stoppingDistance, bool canRun, float completionTime, bool deleteOnCompletion)
    {
        this.behaviour = behaviour;
        this.path = path;
        this.waitingTimeOnPath = waitingTimeOnPath;
        this.stoppingDistance = stoppingDistance;
        this.canRun = canRun;

        autoComplete = true;
        this.completionTime = completionTime;
        this.deleteOnCompletion = deleteOnCompletion;
    }

    #region Save

    public AIPackage(SavedData data)
    {
        behaviour = data.behaviour;

        path = new GameObject[data.pathID.Length];
        System.Reflection.MethodInfo method = typeof(Object).GetMethod("FindObjectFromInstanceID", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);

        for (int i = 0; i < path.Length; i++)
        {
            object obj = method.Invoke(null, new object[] { data.pathID[i] });

            if (obj != null)
                path[i] = obj as GameObject;
            else
                path[i] = null;
        }

        waitingTimeOnPath = data.waitingTimeOnPath;

        stoppingDistance = data.stoppingDistance;
        canRun = data.canRun;

        autoComplete = data.autoComplete;
        completionTime = data.completionTime;
        deleteOnCompletion = data.deleteOnCompletion;
    }

    [System.Serializable]
    public class SavedData
    {
        public AIBehaviour behaviour;
        public int[] pathID;
        public float waitingTimeOnPath;
        public float stoppingDistance;
        public bool canRun;

        public bool autoComplete;
        public float completionTime;
        public bool deleteOnCompletion;

        public SavedData(AIPackage package)
        {
            behaviour = package.behaviour;

            pathID = new int[package.path.Length];

            for (int i = 0; i < pathID.Length; i++)
            {
                pathID[i] = package.path[i].GetInstanceID();
            }

            waitingTimeOnPath = package.waitingTimeOnPath;
            stoppingDistance = package.stoppingDistance;
            canRun = package.canRun;
            autoComplete = package.autoComplete;
            completionTime = package.completionTime;
            deleteOnCompletion = package.deleteOnCompletion;
        }
    }

    #endregion
}
