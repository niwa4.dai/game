using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCharacterData", menuName = "Scriptable Objects/Actors/Character Data")]
public class CharacterDataObject : ScriptableObject
{
    public string firstName;
    public string lastName;
    public Sprite portrait;

    public AttributesStruct attributes;

    public CharacterClassObject[] classes = new CharacterClassObject[0];
    public WeaponDataObject weapon;
}