using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class CharacterBehaviour : MonoBehaviour, IInteractable, ISaveable
{
    public CharacterDataObject data;

    [Tooltip("Cible suivi par la camera epaule")]
    public Transform cameraTarget;
    [Tooltip("Capacite du personnage pour les interactions externes")]
    public CharacterAbility ability = CharacterAbility.None;

    public List<DialogueObject> dialogues = new List<DialogueObject>();

    public AIController aiController = new AIController();

    public UnityAction<IInteractable> onInteractableChange;
    public IInteractable interactableInRange
    {
        get { return _interactableInRange; }
        set
        {
            if (onInteractableChange != null)
                onInteractableChange.Invoke(value);
            _interactableInRange = value;
        }
    }
    private IInteractable _interactableInRange;
    public bool affectedByCinematic
    {
        get { return _affectedByCinematic; }
        set
        {
            if (value == true)
                agent.ResetPath();

            agent.enabled = !value;
            _affectedByCinematic = value;
        }
    }
    private bool _affectedByCinematic;

    public UnityAction onStep;

    public NavMeshAgent agent { get { return _agent; } }
    private NavMeshAgent _agent;
    private Animator animator;

    private const string parameterVelocityX = "Velocity X";
    private const string parameterVelocityZ = "Velocity Z";
    private const string parameterMoving = "Moving";

    public const float walkingSpeed = 1.25f;
    public const float runningSpeed = 4f;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        if (data != null)
            CharacterReferencesManager.AddReference(data, this);

        //aiController.isActive = true;
    }

    //Animator callback
    public void FootL() { MakeStep(); }
    public void FootR() { MakeStep(); }

    private void Update()
    {
        if (affectedByCinematic == true)
            return;

        if (aiController.isActive == false)
            return;

        aiController.EvaluateCurrentPackage(this);

        Vector3 localVelocity = transform.InverseTransformDirection(agent.velocity);
        Vector3 clampedVelocity = Vector3.ClampMagnitude(localVelocity, 1);

        if (agent.speed == walkingSpeed)
            clampedVelocity *= 0.5f;

        if (clampedVelocity.magnitude > 0)
            animator.SetBool(parameterMoving, true);
        else
            animator.SetBool(parameterMoving, false);


        animator.SetFloat(parameterVelocityX, clampedVelocity.x);
        animator.SetFloat(parameterVelocityZ, clampedVelocity.z);
    }

    #region Locomotion

    public void Move(Vector3 _Delta)
    {
        if (affectedByCinematic == true)
            return;

        agent.ResetPath();

        if (_Delta.magnitude > 0)
        {
            agent.transform.rotation = Quaternion.Euler(0, cameraTarget.eulerAngles.y, 0);
            cameraTarget.localEulerAngles = new Vector3(cameraTarget.localEulerAngles.x, 0, 0);

            animator.SetBool(parameterMoving, true);
        }
        else
            animator.SetBool(parameterMoving, false);

        animator.SetFloat(parameterVelocityX, _Delta.x);
        animator.SetFloat(parameterVelocityZ, _Delta.z);
    }

    public void LookAt(Vector3 _Position)
    {
        transform.LookAt(_Position);
    }

    public void Follow(CharacterBehaviour _Target, float _Distance)
    {
        if (affectedByCinematic == true)
            return;

        aiController.isActive = true;

        aiController.ClearPackages();
        aiController.AddPackage(new AIPackage(AIBehaviour.Follow, new GameObject[] { _Target.gameObject }, 0f, _Distance, true));
    }

    #endregion

    private void MakeStep()
    {
        if (onStep != null)
            onStep.Invoke();

        //Play Footstep SFX & VFX
    }

    private void OnAnimatorMove()
    {
        if (affectedByCinematic == true)
            transform.position += animator.deltaPosition;
        else if (aiController.isActive == false)
        {
            //When Game is paused, deltaTime = 0 !
            if (Time.deltaTime > 0)
                agent.velocity = animator.deltaPosition / Time.deltaTime;
            else
                agent.velocity = Vector3.zero;
        }
    }

    public void Activate(CharacterBehaviour _Activator)
    {
        //agent.isStopped = true;

        if (FindValidDialogue(false,  out DialogueObject dialogue) == false)
            return;

        DialogueManager.StartDialogue(dialogue);

        if (dialogue.oneTime == true)
            dialogues.Remove(dialogue);

        if (FindValidDialogue(false, out dialogue) == false)
            RemoveFromInteractableInRange(_Activator);
    }

    public void ToggleAgent(bool _Value)
    {
        //Move(Vector3.zero);
        agent.isStopped = !_Value;
    }

    public void SetAsInteractableInRange(CharacterBehaviour _Character)
    {
        /*if (dialog == null)
            return;*/

        /*if (dialogues.Count == 0)
            return;*/

        /*if (dialogues[0].autoStart == true)
        {
            if (dialogues[0].reducedDisplay == true)
                DialogueManager.StartReducedDialogue(dialogues[0]);
            else
                DialogueManager.StartDialogue(dialogues[0]);

            if (dialogues[0].oneTime == true)
                dialogues.RemoveAt(0);
        }
        else
            _Character.interactableInRange = this;*/

        if (FindValidDialogue(true, out DialogueObject dialogue) == true)
        {
            if (dialogue.reducedDisplay == true)
                DialogueManager.StartReducedDialogue(dialogue);
            else
                DialogueManager.StartDialogue(dialogue);

            if (dialogue.oneTime == true)
                dialogues.Remove(dialogue);
        }
        
        if (FindValidDialogue(false, out dialogue) == true)
            _Character.interactableInRange = this;
    }

    public void RemoveFromInteractableInRange(CharacterBehaviour _Character)
    {
        if (_Character.interactableInRange == (IInteractable)this)
            _Character.interactableInRange = null;
    }

    public void ToggleAnimatorBoolParameter(string _ParameterName, bool _Value)
    {
        animator.SetBool(_ParameterName, _Value);
    }

    private bool FindValidDialogue(bool _AutoStart, out DialogueObject _Output)
    {
        foreach (DialogueObject dialogueObject in dialogues)
        {
            if (dialogueObject.condition.valid == false)
                continue;

            if (dialogueObject.autoStart != _AutoStart)
                continue;

            _Output = dialogueObject;
            return true;
        }

        _Output = null;
        return false;
    }

    #region Save

    [System.Serializable]
    private class SavedData
    {
        public float positionX, positionY, positionZ;
        public float angleX, angleY, angleZ;
        //Add packages... AIPackage.target is not serializable...
        //public List<AIPackage> packages = new List<AIPackage>();
        public List<AIPackage.SavedData> packages = new List<AIPackage.SavedData>();

        public SavedData(Transform transform, List<AIPackage> packages)
        {
            positionX = transform.position.x;
            positionY = transform.position.y;
            positionZ = transform.position.z;
            angleX = transform.eulerAngles.x;
            angleY = transform.eulerAngles.y;
            angleZ = transform.eulerAngles.z;

            foreach (AIPackage package in packages)
            {
                this.packages.Add(new AIPackage.SavedData(package));
            }
        }
    }

    public void Save(BinaryFormatter _BF, FileStream _File)
    {
        SavedData data = new SavedData(transform, aiController.GetPackages());

        _BF.Serialize(_File, data);
    }

    public void Load(BinaryFormatter _BF, FileStream _File)
    {
        SavedData data = (SavedData)_BF.Deserialize(_File);

        if (data == null)
        {
            Debug.Log("No data");
        }

        agent.enabled = false;
        transform.position = new Vector3(data.positionX, data.positionY, data.positionZ);
        transform.eulerAngles = new Vector3(data.angleX, data.angleY, data.angleZ);
        agent.enabled = true;

        List<AIPackage> packages = new List<AIPackage>();

        foreach (AIPackage.SavedData package in data.packages)
        {
            AIPackage newPackage = new AIPackage(package);
            if (newPackage.path.Length > 0)
                packages.Add(new AIPackage(package));
        }

        aiController.SetPackages(this, packages);
    }

    #endregion
}