using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    private static readonly string path = Path.Combine(Application.streamingAssetsPath, "TempSave.data");
    private const KeyCode saveKey = KeyCode.O;
    private const KeyCode loadKey = KeyCode.P;

    [System.Serializable]
    private class SavedData 
    {
        public int stepsBeforeEncounter;

        public SavedData(int stepsBeforeEncounter)
        {
            this.stepsBeforeEncounter = stepsBeforeEncounter;
        }
    }

    public static void LoadAllSaveable()
    {
        try
        {
            BinaryFormatter bf = new BinaryFormatter();

            using (FileStream file = File.Open(path, FileMode.Open))
            {
                foreach (ISaveable savable in FindObjectsOfType<MonoBehaviour>().OfType<ISaveable>())
                {
                    savable.Load(bf, file);
                }

                SavedData data = (SavedData)bf.Deserialize(file);
                EncounterManager.stepsBeforeEncounter = data.stepsBeforeEncounter;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log("Cannot load save " + e); 
        }

        PlayerController.Instance.RefreshPlayableCharacter();
    }

    public static void SaveAllSaveable()
    {
        BinaryFormatter bf = new BinaryFormatter();

        using (FileStream file = File.Open(path, FileMode.OpenOrCreate))
        {
            foreach (ISaveable savable in FindObjectsOfType<MonoBehaviour>().OfType<ISaveable>())
            {
                savable.Save(bf, file);
            }


            SavedData data = new SavedData(EncounterManager.stepsBeforeEncounter);
            bf.Serialize(file, data);
        }

        Debug.Log("Save done");
    }

    private void Update()
    {
        if (Input.GetKeyDown(saveKey))
            SaveAllSaveable();
        if (Input.GetKeyDown(loadKey))
            LoadAllSaveable();
    }
}
