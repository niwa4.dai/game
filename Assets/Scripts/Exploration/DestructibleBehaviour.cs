using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DestructibleBehaviour : MonoBehaviour, ISaveable
{
    [SerializeField]
    private float destructionTime = 1f;
    private bool destroyed = false;

    private Rigidbody[] rigidbodies = new Rigidbody[0];

    private void Awake()
    {
        rigidbodies = GetComponentsInChildren<Rigidbody>(true);
    }

    public void Destroy()
    {
        if (destroyed == true)
            return;

        StartCoroutine(Destruction());
        destroyed = true;
    }

    private IEnumerator Destruction()
    {
        List < Rigidbody > rigidbodiesList = rigidbodies.ToList();

        //float frequence = 0.05f;
        float frequence = destructionTime / rigidbodiesList.Count;
        float oldTime = Time.time - frequence;

        //float randomDelay = frequence;

        while (rigidbodiesList.Count > 0)
        {
            float newTime = Time.time;
            int count = Mathf.FloorToInt((newTime - oldTime) / frequence);

            count = Mathf.Clamp(count, 0, rigidbodiesList.Count);

            for (int i = 0; i < count; i++)
            {
                int index = Random.Range(0, rigidbodiesList.Count);

                rigidbodiesList[index].isKinematic = false;
                rigidbodiesList[index].useGravity = true;


                //rigidbodiesList[index].AddForceAtPosition(Vector3.up, transform.position , ForceMode.Impulse);
                rigidbodiesList[index].AddForce(Vector3.up, ForceMode.Impulse);

                rigidbodiesList.RemoveAt(index);
            }

            //randomDelay = Random.Range(0.05f, 0.2f);

            oldTime = newTime;

            //yield return new WaitForSeconds(randomDelay);
            yield return new WaitForSeconds(Random.Range(0.05f, 0.2f));
        }

        yield return new WaitForSeconds(10f);

        foreach(Rigidbody rb in rigidbodies)
        {
            if (rb.transform.localPosition.y < -50)
                rb.gameObject.SetActive(false);
        }
    }

    #region Save

    [System.Serializable]
    private class SavedData
    {
        public bool destroyed;
        public List<bool> childrenState = new List<bool>();
        public List<RigidbodyData> activeChildren = new List<RigidbodyData>();

        public SavedData(bool destroyed, Rigidbody[] rigidbodies)
        {
            this.destroyed = destroyed;

            foreach(Rigidbody rigidbody in rigidbodies)
            {
                bool activeInHierarchy = rigidbody.gameObject.activeInHierarchy;
                childrenState.Add(activeInHierarchy);

                if (activeInHierarchy == true)
                    activeChildren.Add(new RigidbodyData(rigidbody));
            }
        }

        [System.Serializable]
        public struct RigidbodyData
        {
            public float positionX, positionY, positionZ;
            public float angleX, angleY, angleZ;
            public bool useGravity;

            public float velocityX, velocityY, velocityZ;
            public float angularVelocityX, angularVelocityY, angularVelocityZ;

            public RigidbodyData(Rigidbody rigidbody)
            {
                positionX = rigidbody.transform.position.x;
                positionY = rigidbody.transform.position.y;
                positionZ = rigidbody.transform.position.z;
                angleX = rigidbody.transform.eulerAngles.x;
                angleY = rigidbody.transform.eulerAngles.y;
                angleZ = rigidbody.transform.eulerAngles.z;

                useGravity = rigidbody.useGravity;

                velocityX = rigidbody.velocity.x;
                velocityY = rigidbody.velocity.y;
                velocityZ = rigidbody.velocity.z;
                angularVelocityX = rigidbody.angularVelocity.x;
                angularVelocityY = rigidbody.angularVelocity.y;
                angularVelocityZ = rigidbody.angularVelocity.z;
            }
        }
    }

    public void Save(BinaryFormatter _BF, FileStream _File)
    {
        SavedData data = new SavedData(destroyed, rigidbodies);

        _BF.Serialize(_File, data);
    }

    public void Load(BinaryFormatter _BF, FileStream _File)
    {
        SavedData data = (SavedData)_BF.Deserialize(_File);

        if (data == null)
        {
            Debug.Log("No data");
        }

        destroyed = data.destroyed;

        int activeChildIndex = 0;

        for(int i = 0; i < data.childrenState.Count; i++)
        {
            bool activeInHierarchy = data.childrenState[i];
            rigidbodies[i].gameObject.SetActive(activeInHierarchy);

            if (activeInHierarchy == false)
                continue;

            SavedData.RigidbodyData activeChild = data.activeChildren[activeChildIndex];

            rigidbodies[i].transform.position = new Vector3(activeChild.positionX, activeChild.positionY, activeChild.positionZ);
            rigidbodies[i].transform.eulerAngles = new Vector3(activeChild.angleX, activeChild.angleY, activeChild.angleZ);

            rigidbodies[i].isKinematic = !activeChild.useGravity;
            rigidbodies[i].useGravity = activeChild.useGravity;

            rigidbodies[i].velocity = new Vector3(activeChild.velocityX, activeChild.velocityY, activeChild.velocityZ);
            rigidbodies[i].angularVelocity = new Vector3(activeChild.angularVelocityX, activeChild.angularVelocityY, activeChild.angularVelocityZ);

            activeChildIndex++;
        }
    }

    #endregion
}
