using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Exploration Area", menuName = "Scriptable Objects/Exploration Area")]
public class ExplorationAreaObject : ScriptableObject
{
    new public string name;
    public bool isHostile;
    public MinMaxInt stepsBetweenEncounters;
}
