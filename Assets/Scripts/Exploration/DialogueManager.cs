using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DialogueManager
{
    //private static CharacterBehaviour interlocutor;
    private static Dialogue dialogue;

    public static void StartReducedDialogue(DialogueObject _Dialogue)
    {
        UIManager.Instance.DisplayReducedDialogue(new Dialogue(_Dialogue));
    }

    public static void StartDialogue(/*CharacterBehaviour _Interlocutor, */DialogueObject _Dialogue)
    {
        dialogue = new Dialogue(_Dialogue);

        PlayerController.Instance.TogglePlayerControls(false);

        Vector3 lookAtPoint = Vector3.zero;

        for (int i = 0; i <  dialogue.speakers.Count; i++)
        {
            lookAtPoint += dialogue.speakers[i].transform.position;
        }

        if(dialogue.speakers.Contains(PlayerController.Instance.characterControlled) == false)
        {
            lookAtPoint += PlayerController.Instance.characterControlled.transform.position;
            lookAtPoint /= dialogue.speakers.Count + 1;

            PlayerController.Instance.characterControlled.LookAt(lookAtPoint);
        }
        else
            lookAtPoint /= dialogue.speakers.Count;

        foreach (CharacterBehaviour speaker in dialogue.speakers)
        {
            speaker.ToggleAgent(false);
            speaker.LookAt(lookAtPoint);
        }

        DisplayNextSentence();
    }

    public static void DisplayCurrentSentence(bool _TypingCoroutine = true)
    {
        if (dialogue == null)
            return;

        Sentence sentence = dialogue.GetCurrentSentence();

        if (sentence == null)
            EndDialogue();
        else
            UIManager.Instance.DisplayDialogue(sentence, _TypingCoroutine);
    }

    /*public static void DisplayNextSentence()
    {
        if (dialogue == null)
            return;

        Sentence sentence = dialogue.GetNextSentence();

        if (sentence == null)
            EndDialogue();
        else
            UIManager.Instance.DisplayDialogue(sentence, true);
    }*/

    public static void DisplayNextSentence()
    {
        if (dialogue == null)
            return;

        dialogue.GetNextSentence();
        DisplayCurrentSentence(true);
    }

    private static void EndDialogue()
    {
        UIManager.Instance.HideDialogue();

        PlayerController.Instance.TogglePlayerControls(true);

        foreach (CharacterBehaviour speaker in dialogue.speakers)
        {
            speaker.ToggleAgent(true);
        }

        dialogue = null;
    }
}

[System.Serializable]
public class Dialogue
{
    //public string name;
    public List<CharacterBehaviour> speakers
    {
        get
        {
            if(_speakers == null)
            {
                List<CharacterDataObject> characterDatas = new List<CharacterDataObject>();

                foreach (Sentence sentence in dialogueObject.sentences)
                {
                    if (characterDatas.Contains(sentence.speaker) == false)
                        characterDatas.Add(sentence.speaker);
                }

                _speakers = new List<CharacterBehaviour>();

                foreach (CharacterDataObject characterData in characterDatas)
                {
                    _speakers.Add(CharacterReferencesManager.GetReference(characterData));
                }
            }

            return _speakers;
        }
    }
    private List<CharacterBehaviour> _speakers;

    private DialogueObject dialogueObject;
    private int currentSentenceIndex = -1;

    public Dialogue(DialogueObject dialogueObject)
    {
        this.dialogueObject = dialogueObject;
    }

    public Sentence GetCurrentSentence()
    {
        if (currentSentenceIndex >= dialogueObject.sentences.Length)
            return null;

        Sentence sentence = dialogueObject.sentences[currentSentenceIndex];

        CharacterBehaviour speaker = CharacterReferencesManager.GetReference(sentence.speaker);
        speaker.ToggleAnimatorBoolParameter(sentence.animatorParameter, true);

        return sentence;
    }

    /*public Sentence GetNextSentence()
    {
        if(currentSentenceIndex > 0)
        {
            Sentence previousSentence = dialogueObject.sentences[currentSentenceIndex - 1];

            foreach(QuestUpdate consequence in previousSentence.consequences)
            {
                consequence.UpdateQuest();
            }
        }

        if (currentSentenceIndex < dialogueObject.sentences.Length)
        {
            Sentence nextSentence = dialogueObject.sentences[currentSentenceIndex];
            currentSentenceIndex++;
            return nextSentence;
        }
        else
            return null;
    }*/

    public Sentence GetNextSentence()
    {
        if (currentSentenceIndex >= 0)
        {
            Sentence previousSentence = dialogueObject.sentences[currentSentenceIndex];

            CharacterBehaviour speaker = CharacterReferencesManager.GetReference(previousSentence.speaker);
            speaker.ToggleAnimatorBoolParameter(previousSentence.animatorParameter, false);

            foreach (QuestUpdate consequence in previousSentence.consequences)
            {
                consequence.UpdateQuest();
            }
        }

        currentSentenceIndex++;

        return GetCurrentSentence();
        /*if (currentSentenceIndex < dialogueObject.sentences.Length)
        {
            return GetCurrentSentence();
        }
        else
            return null;*/
    }
}