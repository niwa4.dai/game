using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDialogue", menuName = "Scriptable Objects/Dialogue")]
public class DialogueObject : ScriptableObject
{
    public QuestCondition condition;

    public Sentence[] sentences;
    public bool oneTime;
    public bool autoStart;
    public bool reducedDisplay;
}

[System.Serializable]
public class Sentence
{
    public CharacterDataObject speaker;
    [TextArea(3, 10)]
    public string text;
    public string animatorParameter;

    public QuestUpdate[] consequences = new QuestUpdate[0];
}