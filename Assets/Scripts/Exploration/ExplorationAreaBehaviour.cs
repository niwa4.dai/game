using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplorationAreaBehaviour : MonoBehaviour
{
    public static ExplorationAreaObject currentExplorationArea { get; private set; }

    [SerializeField]
    private ExplorationAreaObject explorationArea;
    [SerializeField]
    private Camera[] mapCameras;
    [SerializeField]
    private Vector4 mapLimits;

    private bool explored = false;

    public void OnEnter()
    {
        if (currentExplorationArea != explorationArea)
        {
            UIManager.Instance.DisplayExplorationAreaPopUp(explorationArea.name);
            EncounterManager.UpdateEncountersInArea(explorationArea);

            currentExplorationArea = explorationArea;

            if (explored == false)
            {
                explored = true;

                foreach (Camera cam in mapCameras)
                {
                    MapRenderer.Instance.ExpandMap(explorationArea, new MapCamera(cam, mapLimits));
                }
            }
        }
    }
}
