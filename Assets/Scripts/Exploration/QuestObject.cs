using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewQuest", menuName = "Scriptable Objects/Quest")]
public class QuestObject : ScriptableObject { }