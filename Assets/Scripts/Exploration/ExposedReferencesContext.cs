using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExposedReferencesContext : MonoBehaviour, IExposedPropertyTable
{
    public DialogueObject asset;

    public List<PropertyName> propertyNamesList = new List<PropertyName>();
    public List<Object> referencesList = new List<Object>();

    public void ClearReferenceValue(PropertyName id)
    {
        int index = propertyNamesList.IndexOf(id);

        if (index >= 0)
        {
            propertyNamesList.RemoveAt(index);
            referencesList.RemoveAt(index);
        }
    }

    public Object GetReferenceValue(PropertyName id, out bool idValid)
    {
        int index = propertyNamesList.IndexOf(id);

        if (index >= 0)
        {
            idValid = true;
            return referencesList[index];
        }

        idValid = false;
        return null;
    }

    public void SetReferenceValue(PropertyName id, Object value)
    {
        int index = propertyNamesList.IndexOf(id);

        if (index >= 0)
        {
            referencesList[index] = value;
        }else
        {
            propertyNamesList.Add(id);
            referencesList.Add(value);
        }
    }
}
