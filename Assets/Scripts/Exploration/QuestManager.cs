using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class QuestManager
{
    private static Dictionary<QuestObject, Quest> quests;

    public static Quest GetQuest(QuestObject _QuestObject)
    {
        if (quests == null)
            InitQuestsDictionary();

        quests.TryGetValue(_QuestObject, out Quest output);

        return output;
    }

    private static void InitQuestsDictionary()
    {
        quests = new Dictionary<QuestObject, Quest>();

        foreach (QuestObject questObject in Resources.LoadAll<QuestObject>(""))
        {
            quests.Add(questObject, new Quest());
        }
    }
}

public class Quest
{
    public int stage { get; private set; }
    public UnityAction<int> onStageChange;

    public void SetStage(int _Stage)
    {
        stage = _Stage;
        onStageChange.Invoke(stage);
    }
}

[System.Serializable]
public class QuestUpdate
{
    public QuestObject quest;
    public enum UpdateMode
    {
        Set,Increment
    }
    public UpdateMode mode;
    public int stage;

    public void UpdateQuest()
    {
        Quest questToUpdate = QuestManager.GetQuest(quest);

        if (mode == UpdateMode.Set)
            questToUpdate.SetStage(stage);
        else if (mode == UpdateMode.Increment)
            questToUpdate.SetStage(questToUpdate.stage + stage);
    }
}

[System.Serializable]
public class QuestCondition
{
    public QuestObject quest;
    public Comparator comparator;
    public int stage;

    public bool valid 
    {
        get 
        {
            if (quest == null)
                return true;
            else
                return CheckCondition();
        }
    }
    //private bool _valid;

    /*public void UpdateStage(int _Stage)
    {
        _valid = CheckCondition(_Stage);
    }*/

    private bool CheckCondition()
    {
        int currentStage = QuestManager.GetQuest(quest).stage;

        switch (comparator)
        {
            case Comparator.Greater:
                return (currentStage > stage);
            case Comparator.Less:
                return (currentStage < stage);
            case Comparator.Equals:
                return (currentStage == stage);
            case Comparator.NotEqual:
                return (currentStage != stage);
            default:
                return false;
        }
    }

    /*private bool CheckCondition(int _Stage)
    {
        switch (comparator)
        {
            case Comparator.Greater:
                return (_Stage > stage);
            case Comparator.Less:
                return (_Stage < stage);
            case Comparator.Equals:
                return (_Stage == stage);
            case Comparator.NotEqual:
                return (_Stage != stage);
            default:
                return false;
        }
    }*/

    /*public bool CheckCondition(int _Stage)
    {
        switch (comparator)
        {
            case Comparator.Greater:
                return (_Stage > stage);
            case Comparator.Less:
                return (_Stage < stage);
            case Comparator.Equals:
                return (_Stage == stage);
            case Comparator.NotEqual:
                return (_Stage != stage);
            default:
                return false;
        }
    }*/
}