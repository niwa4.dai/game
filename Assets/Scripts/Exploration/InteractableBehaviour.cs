using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class InteractableBehaviour : MonoBehaviour, IInteractable, ISaveable
{
    public bool activated { get; private set; }
    public bool activable { get { return ((activated == false || onTimeUse == false) && condition.valid); } }

    [SerializeField]
    private CharacterAbility abilityNeeded = CharacterAbility.None;
    [SerializeField]
    private bool onTimeUse = false;

    [SerializeField]
    private QuestCondition condition;

    [Header("Consequences")]

    [SerializeField]
    private DialogueObject dialogueConsequence;
    [SerializeField]
    private QuestUpdate questConsequence;

    private const string interactableValidLayer = "InteractableValid";
    private const string interactableUnvalidLayer = "InteractableUnvalid";
    private PlayableDirector director;

    private void Awake()
    {
        director = GetComponent<PlayableDirector>();
        activated = false;

        ToggleVisualFeedback(false);

        PlayerController.Instance.onCharacterChangeAction += (CharacterBehaviour character) =>
        {
            if (activable == true)
                ToggleVisualFeedback(true);
        };

        if (condition.quest != null)
        {
            QuestManager.GetQuest(condition.quest).onStageChange += (int stage) =>
            {
                //condition.UpdateStage(stage);
                if (activable == true)
                    ToggleVisualFeedback(true);
            };
        }
    }

    public void Activate(CharacterBehaviour _Activator)
    {
        if (activable == false)
            return;

        activated = !activated;

        if (director != null)
        {
            Dictionary<PlayableBinding, Object> defaultBinding = new Dictionary<PlayableBinding, Object>();
            //_Activator.transform.SetParent(transform);
            foreach (PlayableBinding playableAssetOutput in director.playableAsset.outputs)
            {
                defaultBinding.Add(playableAssetOutput, director.GetGenericBinding(playableAssetOutput.sourceObject));

                if (playableAssetOutput.streamName == "ActivatorAnimatorTrack")
                {
                    Animator dummy = (Animator)defaultBinding[playableAssetOutput];
                    _Activator.transform.position = dummy.transform.position;
                    _Activator.transform.rotation = dummy.transform.rotation;

                    director.SetGenericBinding(playableAssetOutput.sourceObject, _Activator.GetComponent<Animator>());
                }
                else if (playableAssetOutput.streamName.Contains("ActivatorSignalTrack"))
                {
                    director.SetGenericBinding(playableAssetOutput.sourceObject, _Activator.GetComponent<SignalReceiver>());
                }
            }

            director.Play();

            StartCoroutine(WaitForDirectorEnd(director, () =>
            {
                foreach (KeyValuePair<PlayableBinding, Object> pair in defaultBinding)
                {
                    director.SetGenericBinding(pair.Key.sourceObject, pair.Value);
                }

                UpdateConsequences();
            }));
        }


        if (activable == false)
        {
            ToggleVisualFeedback(false);
            RemoveFromInteractableInRange(_Activator);
        }

        UpdateConsequences();
    }

    public void SetAsInteractableInRange(CharacterBehaviour _Character)
    {
        if (activable == false)
            return;

        if (CanActivate(_Character) == false)
            return;

        _Character.interactableInRange = this;
    }

    public void RemoveFromInteractableInRange(CharacterBehaviour _Character)
    {
        if (_Character.interactableInRange == (IInteractable)this)
            _Character.interactableInRange = null;
    }

    private void ToggleVisualFeedback(bool _Value)
    {
        int layer = LayerMask.NameToLayer("Default");

        if (_Value == true)
        {
            if (CanActivate(PlayerController.Instance.characterControlled))
                layer = LayerMask.NameToLayer(interactableValidLayer);
            else
                layer = LayerMask.NameToLayer(interactableUnvalidLayer);
        }

        foreach (Transform child in GetComponentsInChildren<Transform>())
        {
            child.gameObject.layer = layer;
        }
    }

    private bool CanActivate(CharacterBehaviour _Character)
    {
        return abilityNeeded == CharacterAbility.None || _Character.ability == abilityNeeded;
    }

    private void UpdateConsequences()
    {
        if (questConsequence.quest != null)
            questConsequence.UpdateQuest();
        if (dialogueConsequence != null)
            DialogueManager.StartDialogue(dialogueConsequence);
    }

    private IEnumerator WaitForDirectorEnd(PlayableDirector _Director, System.Action _Action)
    {
        yield return null;

        while (_Director.state == PlayState.Playing) yield return null;

        _Action();
    }

    #region Save

    [System.Serializable]
    private class SavedData
    {
        public bool activated;
        public double directorTimer;

        public SavedData(bool activated, double directorTimer)
        {
            this.activated = activated;
            this.directorTimer = directorTimer;
        }
    }

    public void Save(BinaryFormatter _BF, FileStream _File)
    {
        double time = 0f;

        if (director != null)
        {
            if (director.state == PlayState.Playing)
                time = director.time;
            else if (activated == true)
                time = director.playableAsset.duration;
        }

        SavedData data = new SavedData(activated, time);

        _BF.Serialize(_File, data);
    }

    public void Load(BinaryFormatter _BF, FileStream _File)
    {
        SavedData data = (SavedData)_BF.Deserialize(_File);

        if (data == null)
        {
            Debug.Log("No data");
        }

        activated = data.activated;
        ToggleVisualFeedback(activable);

        if (director == null)
            return;

        director.time = data.directorTimer;
        director.Evaluate();

        if (director.time <= 0)
            director.Stop();
        else
        {
            director.Play();
        }
    }

    #endregion
}
