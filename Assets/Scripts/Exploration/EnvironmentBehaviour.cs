using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnvironmentBehaviour : MonoBehaviour
{
    public QuestCondition condition;
    public UnityEvent consequence;

    private void Awake()
    {
        if (condition.quest != null)
        {
            QuestManager.GetQuest(condition.quest).onStageChange += (int stage) =>
            {
                if (condition.valid == true)
                    consequence.Invoke();
            };
        }
    }

    public void Play()
    {
        Animation animation = GetComponent<Animation>();

        if (animation != null)
            animation.Play();
    }
}
