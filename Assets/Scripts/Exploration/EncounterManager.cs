using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class EncounterManager
{
    public static int stepsBeforeEncounter
    {
        get { return _stepsBeforeEncounter; }
        set
        {
            if (ExplorationAreaBehaviour.currentExplorationArea.isHostile == false)
                return;

            if (value <= 0)
            {
                SetRandomSteps(ExplorationAreaBehaviour.currentExplorationArea);
                StartEncounter();
            }
            else _stepsBeforeEncounter = value;
        }
    }
    private static int _stepsBeforeEncounter = -1;

    public static void StartEncounter()
    {
        Debug.Log("Enemy encounter");

        //SaveManager.SaveAllSavable();

        //Load encounter scene
        //SceneManager.LoadScene("NiwaGame/Combat/Squelette CMB");
    }

    public static void UpdateEncountersInArea(ExplorationAreaObject _NewArea)
    {
        ExplorationAreaObject oldArea = ExplorationAreaBehaviour.currentExplorationArea;

        if (_NewArea == oldArea)
            return;

        if (_NewArea.isHostile == true)
        {
            if (_stepsBeforeEncounter < 0 || oldArea == null)
                SetRandomSteps(_NewArea);
            else if (oldArea.isHostile == true)
            {
                float minStepsRatio = (float)_NewArea.stepsBetweenEncounters.min / (float)oldArea.stepsBetweenEncounters.min;
                float maxStepsRatio = (float)_NewArea.stepsBetweenEncounters.max / (float)oldArea.stepsBetweenEncounters.max;
                float stepsRatio = (minStepsRatio + maxStepsRatio) * 0.5f;

                _stepsBeforeEncounter = Mathf.FloorToInt(_stepsBeforeEncounter * stepsRatio);
            }
        }
    }

    private static void SetRandomSteps(ExplorationAreaObject _Area)
    {
        _stepsBeforeEncounter = Random.Range(_Area.stepsBetweenEncounters.min, _Area.stepsBetweenEncounters.max);
        Debug.Log("Next encounter in " + _stepsBeforeEncounter + " steps");
    }
}
