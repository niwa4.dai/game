using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class AreaTriggerBehaviour : MonoBehaviour
{
    [SerializeField]
    private UnityEvent<CharacterBehaviour> enterEvent, exitEvent;

    [SerializeField]
    private string tagTrigger;
    [SerializeField]
    private LayerMask layerTrigger;


    private void OnTriggerEnter(Collider _Other)
    {
        if (enterEvent == null || enterEvent.GetPersistentEventCount() == 0)
            return;
        
        if (IsColliderValid(_Other, out CharacterBehaviour character) == false)
            return;

        enterEvent.Invoke(character);
    }

    private void OnTriggerExit(Collider _Other)
    {
        if (exitEvent == null || exitEvent.GetPersistentEventCount() == 0)
            return;

        if (IsColliderValid(_Other, out CharacterBehaviour character) == false)
            return;

        exitEvent.Invoke(character);
    }

    private bool IsColliderValid(Collider _Collider, out CharacterBehaviour _Character)
    {
        _Character = null;

        if (layerTrigger != (layerTrigger | (1 << _Collider.gameObject.layer)))
            return false;
        
        if (string.IsNullOrEmpty(tagTrigger) == false && _Collider.tag != tagTrigger)
            return false;

        _Character = _Collider.GetComponent<CharacterBehaviour>();

        if (_Character == null)
            return false;
        else
            return true;

    }
}
