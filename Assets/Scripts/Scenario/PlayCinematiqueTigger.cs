﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Slate;
using TPCWC;
using UnityEngine.Events;

public class PlayCinematiqueTigger : MonoBehaviour
{
    public string eventName;
    public Cutscene scene;
    private bool played;

    public List<string> aJouerApresEvenements;


    public UnityEvent eventsAFaire;
 
    // Start is called before the first frame update
    void Start()
    {
        MyEvenement myEvent = EvenementManager.Instance.FindEvenement(eventName);
        if(myEvent == null)
            return;
        eventsAFaire = myEvent.events;
        scene = myEvent.cutscene;
        played = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter(Collider other) {
        bool canplay = canPlayCinematique();
        Debug.Log("canplay = "+canplay);
        if(!canplay)
            return;
        if(canplay && PlayerController.Instance.playerGroup[0].gameObject == other.gameObject  && !played){
            played = true;
            EvenementManager.Instance.addEvent(eventName);
            Debug.Log("on fait une action");
            eventsAFaire.Invoke();
            if(scene != null)
                scene.Play();        
        }
    }

    private bool canPlayCinematique(){
        if(!EvenementManager.Instance.gameData.cinematiqueActive){
            Debug.Log("Cinematique desactivé");
            return false;
        }
        if(EvenementManager.Instance.containsEvent(eventName)){
            Debug.Log("L'evenement n'est pas passe : "+eventName+" : "+EvenementManager.Instance.containsEvent(eventName));
            return false;
        }
        if(!evenementPasseSontPresent()){
            Debug.Log("Les eveneent passer ne sont pas present");
            return false;

        }
        return true;
    }

    private bool evenementPasseSontPresent(){
        foreach (var item in aJouerApresEvenements)
        {
            if(!EvenementManager.Instance.containsEvent(item))
                return false ; 
        }
        return true;
    }
}
