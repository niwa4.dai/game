﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TPCWC;
using Slate;
using UnityEngine.Playables;
using UnityEngine.Events;

public class EvenementManager : Singleton<EvenementManager>
{
    public GameData gameData;
    public Text text;
    public Cutscene firstCutscene;
    public List<CharacterBehaviourAi> personnages;

    public List<MyEvenement> allEvents;

    private void Start() {
        if(gameData.newGame){
            firstCutscene.Play();
            gameData.evenements = new List<string>();
        }
    }

    private void Update() {
        if(Input.GetKeyDown("b")){
            LoadAllEvent();
		}
    }

    public void addEvent(string e){
        if(!gameData.evenements.Contains(e))
            gameData.evenements.Add(e);
    }

    public bool containsEvent(string e){
        return gameData.evenements.Contains(e);
    }

    public void clearEvents(){
         gameData.evenements = new List<string>();
    }

    public void DisplayDialogue(string s, float time){
        StartCoroutine(DisplayDialogueWithTime(s,time));
    }
	IEnumerator DisplayDialogueWithTime(string s,float time){
        text.text = s;
    	yield return new WaitForSeconds(time);
        text.text = "";
	}
    
    public void DisplayObject(GameObject obj){
        obj.SetActive(true);
    }
    public void HideObject(GameObject obj){
        obj.SetActive(false);
    }

    public void FaitBougerLesPerso(int i){
        foreach (CharacterBehaviourAi item in personnages)
        {
            item.Play(i);
        }
    } 

    public MyEvenement FindEvenement(string s){
        foreach (MyEvenement item in allEvents)
        {
            if(item.name == s)
                return item;
        }
        return null;
    }

    public void LoadAllEvent(){
        foreach (string item in gameData.evenements)
        {
            FindEvenement(item).events.Invoke();
        }
    }
}


[System.Serializable]
public class MyEvenement
{
    public string name;
    public Cutscene cutscene;
    public UnityEvent events;
}
