﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WindowReplaceObject : EditorWindow
{
    private GameObject go;
    private string nameOverride;
    //private Vector3 rotationOffset;

    [MenuItem("Window/Replace Object")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(WindowReplaceObject));
    }

    void OnGUI()
    {
        go = EditorGUILayout.ObjectField("Object", go, typeof(GameObject), false) as GameObject;

        nameOverride = EditorGUILayout.TextField("Name Override", nameOverride);
        //rotationOffset = EditorGUILayout.Vector3Field("Rotation Offset", rotationOffset);

        if (GUILayout.Button("Replace"))
        {
            if (Selection.transforms.Length == 0)
                return;
            if (go == null)
                return;

            int count = 0;

            while(Selection.transforms.Length > 0)
            {
                PlaceObject(go, Selection.transforms[0], name, true);

                count++;
            }

            if (count == 0)
                Debug.Log("No objects added");
            else
                Debug.Log(count + " objects added");
        }

        if (GUILayout.Button("Place as child"))
        {
            if (Selection.transforms.Length == 0)
                return;
            if (go == null)
                return;

            int count = 0;

            foreach( Transform transform in Selection.transforms)
            {
                PlaceObject(go, transform, name, false);
                count++;
            }

            if (count == 0)
                Debug.Log("No objects added");
            else
                Debug.Log(count + " objects added");
        }
    }

    static void PlaceObject(GameObject _ObjectToPlace, Transform _Target, string _Name, bool _Replace)
    {
        Transform parent = (_Replace == true)? _Target.parent: _Target;

        GameObject newGO = PrefabUtility.InstantiatePrefab(_ObjectToPlace, parent) as GameObject;
        Undo.RegisterCreatedObjectUndo(newGO, "Place Object");

        if (string.IsNullOrEmpty(_Name) == false)
            newGO.name = _Name;
        else
        {
            var mods = new List<PropertyModification>(PrefabUtility.GetPropertyModifications(newGO));
            for (int j = 0; j < mods.Count; j++)
            {
                if (mods[j].propertyPath == "m_Name")
                {
                    mods.RemoveAt(j);
                    break;
                }
            }
            PrefabUtility.SetPropertyModifications(newGO, mods.ToArray());
        }

        PrefabUtility.RecordPrefabInstancePropertyModifications(newGO);

        newGO.transform.position = _Target.position;
        newGO.transform.rotation = _Target.rotation;
        newGO.transform.localScale = _Target.localScale;

        PrefabUtility.RecordPrefabInstancePropertyModifications(newGO.transform);

        if (_Replace == true)
            Undo.DestroyObjectImmediate(_Target.gameObject);
    }
}
