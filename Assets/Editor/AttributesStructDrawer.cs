using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(AttributesStruct))]
public class AttributesStructDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        // use the default property height, which takes into account the expanded state
        if (property.isExpanded)
            return EditorGUIUtility.singleLineHeight * (System.Enum.GetValues(typeof(ActorAttribute)).Length +1);
        else
            return EditorGUIUtility.singleLineHeight;
    }

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        Rect foldoutRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

        property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label);

        if (property.isExpanded)
        {
            EditorGUI.indentLevel++;

            SerializedProperty attributesValues = property.FindPropertyRelative("attributesValues");

            System.Array attributes = System.Enum.GetValues(typeof(ActorAttribute));
            attributesValues.arraySize = attributes.Length;

            foreach (int i in attributes)
            {
                Rect attributeRect = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight * (i+1), position.width, EditorGUIUtility.singleLineHeight);

                EditorGUI.PropertyField(attributeRect, attributesValues.GetArrayElementAtIndex(i), new GUIContent(((ActorAttribute)i).ToString()));
            }

            EditorGUI.indentLevel--;
        }

        EditorGUI.EndProperty();
        EditorUtility.SetDirty(property.serializedObject.targetObject);
    }
}
