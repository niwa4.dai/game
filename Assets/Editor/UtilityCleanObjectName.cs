﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UtilityCleanObjectName
{
    [MenuItem("Utility/Clean object name")]
    public static void CleanObjectName()
    {
        foreach (Transform transform in Selection.transforms)
        {
            Undo.RecordObject(transform.gameObject, "Clean Object Name");

            var mods = new List<PropertyModification>(PrefabUtility.GetPropertyModifications(transform.gameObject));
            for (int j = 0; j < mods.Count; j++)
            {
                if (mods[j].propertyPath == "m_Name")
                {
                    mods.RemoveAt(j);
                    break;
                }
            }
            PrefabUtility.SetPropertyModifications(transform.gameObject, mods.ToArray());
            PrefabUtility.RecordPrefabInstancePropertyModifications(transform.gameObject);
        }
    }
}
