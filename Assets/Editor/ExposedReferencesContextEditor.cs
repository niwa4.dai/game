using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ExposedReferencesContext))]
public class ExposedReferencesContextEditor : Editor
{
    SerializedProperty asset;
    SerializedObject assetSerializedObject;

    private void OnEnable()
    {
        asset = serializedObject.FindProperty("asset");
        
        if (asset != null && asset.objectReferenceValue != null)
        {
            assetSerializedObject = new SerializedObject(asset.objectReferenceValue, this.target);
        }
        else
            assetSerializedObject = null;
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        base.DrawDefaultInspector();

        if(asset == null)
        {
            asset = serializedObject.FindProperty("asset");
        }

        if (EditorGUI.EndChangeCheck())
        {
            if (asset != null && asset.objectReferenceValue != null)
            {
                assetSerializedObject = new SerializedObject(asset.objectReferenceValue, this.target);
            }
            else
                assetSerializedObject = null;
        }

        if(assetSerializedObject != null)
        {
            SerializedProperty iterator = assetSerializedObject.GetIterator();

            bool enterChildren = true;
            while (iterator.NextVisible(enterChildren))
            {
                enterChildren = false;
                EditorGUILayout.PropertyField(iterator, true);
            }
        }
    }
}
