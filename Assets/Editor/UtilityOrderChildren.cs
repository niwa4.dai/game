﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class UtilityOrderChildren
{
    [MenuItem("Utility/Sort Children")]
    public static void SortChildren()
    {
        if (Selection.activeTransform == null)
            return;

        if (PrefabUtility.IsPartOfAnyPrefab(Selection.activeTransform.gameObject))
            return;

        List<Transform> children = new List<Transform>();

        for (int i = 0; i <  Selection.activeTransform.childCount; i++)
        {
            children.Add(Selection.activeTransform.GetChild(i));
        }

        children = children.OrderBy(t => t.name).ToList();

        for (int i = 0; i < children.Count; i++)
        {
            children[i].SetAsLastSibling();
        }
    }
}
