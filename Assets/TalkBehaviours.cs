using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TalkBehaviours : MonoBehaviour
{
    public string dialogue;
    public List<string> dialogues;
    public Animator animator;
    public AnimationClip talkAnim;
    public float time;
    private bool canTalk = true;
    public bool auto;
    public bool stopPlayer;
    public bool oneTime;
    private bool played;
    public UnityEvent eventApresParler;

    private void OnTriggerStay(Collider other) {
        if(PlayerController.Instance.playerGroup[0].gameObject == other.gameObject && Input.GetKeyDown("e") && canTalk){
            if(played && oneTime)
                return;
            played = true;
            if(animator != null)
                animator.gameObject.transform.LookAt(other.transform);
            if(dialogues.Count == 0)
                StartCoroutine(Talk());
            else
                StartCoroutine(Talks());
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(PlayerController.Instance.playerGroup[0].gameObject == other.gameObject && auto && canTalk){
             if(played && oneTime)
                return;
            played = true;
            if(animator != null)
                animator.gameObject.transform.LookAt(other.transform);
            if(dialogues.Count == 0)
                StartCoroutine(Talk());
            else
                StartCoroutine(Talks());
        }
    }
	IEnumerator Talk(){
        if(stopPlayer){
            PlayerController.Instance.enabled = false;
            PlayerController.Instance.characterControlled.Move(Vector3.zero);
        }
        canTalk = false;
        if(animator != null)
            animator.SetTrigger("Talk");
        FindObjectOfType<EvenementManager>().DisplayDialogue(dialogue,time);
    	yield return new WaitForSeconds(time);
        canTalk = true;
        eventApresParler.Invoke();
        if(stopPlayer)
            PlayerController.Instance.enabled = true;

	}

    IEnumerator Talks(){
        if(stopPlayer){
            PlayerController.Instance.enabled = false;
            PlayerController.Instance.characterControlled.Move(Vector3.zero);
        }
        foreach (var item in dialogues)
        {
            canTalk = false;
            FindObjectOfType<EvenementManager>().DisplayDialogue(item,time);
            if(animator != null)
                animator.SetTrigger("Talk");
            yield return new WaitForSeconds(time);
        }
        canTalk = true;
        eventApresParler.Invoke();
    
        if(stopPlayer)   
            PlayerController.Instance.enabled = true;

	}
}
