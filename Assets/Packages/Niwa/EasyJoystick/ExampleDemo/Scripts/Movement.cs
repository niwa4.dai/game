﻿using UnityEngine;
using System.Collections;
using TPCWC;
public class Movement : MonoBehaviour 
{
	public float speed = 5;
	public EasyJoystick moveJoy, rotateJoy;
	public Transform Gun, bulletSpawn;
	public Rigidbody bulletPrefab;
	public float attck_rate;
	private float delay;
		CameraController cam; 
		private Animator animator;

	private void Start() {
					cam = CameraController.instance;
		animator = GetComponentInChildren<Animator>();
	}

	void FixedUpdate (){
		//Vector3 vert =  moveJoy.MoveInput () * speed * cam.transform.forward;
		//Vector3 horz =  moveJoy.MoveInput () * speed * cam.transform.right;
		Vector3 myMove =moveJoy.MoveInput ();
		
		if(myMove.z > 0.1f || myMove.z < -0.1f){
			GetComponent<Rigidbody>().velocity = moveJoy.MoveInput () * 3;	
		//	animator.SetBool("run", run);
			animator.SetFloat("vertical", 1);
		}else{
				animator.SetFloat("vertical", 0);

		}

		/*if(myMove.z > 0.2f)
			GetComponent<Rigidbody>().velocity = cam.transform.forward * speed;
		if(myMove.z < -0.2f)
			GetComponent<Rigidbody>().velocity = cam.transform.forward * -speed;	
		if(myMove.x > 0.2f)
			GetComponent<Rigidbody>().velocity = cam.transform.right * speed;	
		if(myMove.x < -0.2f)
			GetComponent<Rigidbody>().velocity = cam.transform.right * -speed;	*/


	/*	Vector3 PlayerTargetDirection =  cam.transform.forward;
			//set the y direction to '0' to make sure
			//player don't rotate at Y axis lol :P
			PlayerTargetDirection.y = 0;

			if(PlayerTargetDirection == Vector3.zero)
				//we set it to player's current forward vector!
				PlayerTargetDirection = transform.forward;

			//now it's time to set the rotation of player
			Quaternion targetRot = Quaternion.LookRotation(PlayerTargetDirection);
			Quaternion PlayerTargetRotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * 10f);
			transform.rotation = PlayerTargetRotation;*/
				//Move rigidbody;
	//	moveJoy.Rotate (transform, 15.0F);							//Rotate rigidbody;
	//	rotateJoy.Rotate (Gun, 15.0F);								//Rotate gun;
	}

	void Shoot(){
		if(Time.time > delay){
			Rigidbody bullet = (Rigidbody)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.localRotation);
			bullet.AddForce(bulletSpawn.forward * 1000);
			delay = Time.time + attck_rate;
		}
	}
}
 