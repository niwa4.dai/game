﻿using UnityEngine;
using System.Collections;

namespace ThirdPersonCamera
{
    [RequireComponent(typeof(CameraController)), RequireComponent(typeof(FreeForm))]
    public class LockOnTarget : MonoBehaviour
    {
        
        public float speed;
        private Quaternion newRotation;

        private CameraController cc;
        private FreeForm ff;
        public EasyJoystick rotateJoy;

        void Start()
        {
            newRotation = transform.rotation;
            cc = GetComponent<CameraController>();
        }

        void Update()
        {
            newRotation *= Quaternion.Euler(Vector3.up * (rotateJoy.MoveInput().x)*speed);
            cc.transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, 2f); 

    
        }
    }
}