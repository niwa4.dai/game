﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCharacterSystem : MonoBehaviour
{
    private CharacterController controller
    {
        get
        {
            return GetComponent<CharacterController>();
        }
    }

    private Animator Animacion;
    public ReciveDaño Daño;
    private float aburrido;
    public float Vida = 100;
    public int arma;
    private float solapa = 0;
    private bool ModoDeAsalto, unavezModoDeAsalto;
    private bool unavez;

    public enum Direcciones
    {
        Ninguna,
        Adelante,
        Derecha,
        Izquierda,
        Atras
    }

    public enum ReciveDaño
    {
        Ninguno = 0,
        DañoDebil = 1,
        DañoFuerte = 2,
        DañoBrutal = 3,
        DañoDebilAdelante = 4,
        DañoFuerteAdelante = 5,
        DañoBrutalAdelante = 6,
        DañoDebilDerecha = 7,
        DañoFuerteDerecha = 8,
        DañoBrutalDerecha = 9,
        DañoDebilIzquierda = 10,
        DañoFuerteIzquierda = 11,
        DañoBrutalIzquierda = 12,
        DañoDebilAtras = 13,
        DañoFuerteAtras = 14,
        DañoBrutalAtras = 15,
        DañoPuntapie = 16,
        DañoTropiezo = 17,
        DañoEscudo = 18
    };

    void Start()
    {
        Animacion = GetComponent<Animator>();
    }

    private void Update()
    {
        Mover();
        Rotacion();
        ControlCamara();
        Animaciones();
        
        ActivarModoDeAsalto();
        DesenvainarArmas();
    }

    private void Mover()
    {
        //Movimiento en el Aire
        if (GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("En el Aire"))
        {
            Vector3 motion = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            motion = transform.TransformDirection(motion);
            controller.Move(motion * Time.deltaTime * 7);
        }
        //Movimiento de Gravedad
        if (!GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Despegar"))
        {
            CharacterController[] controles = GetComponentsInChildren<CharacterController>();

            for (int i = 0; i < controles.Length; i++)
                controles[i].Move(new Vector3(0, -1, 0) * Time.deltaTime);
        }
    }

    private void OnDrawGizmosSelected()
    {
        //Vector Arriba
        Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + 3.2f, transform.position.z), transform.TransformDirection(Vector3.up), Color.red);
        //Vector Adelante
        Debug.DrawRay(transform.TransformDirection(new Vector3(transform.position.x, transform.position.y + 2.5f, transform.position.z + 0.75f)), transform.TransformDirection(Vector3.forward), Color.blue); 
    }

    private void Animaciones()
    {

        if (Input.GetAxis("Vertical") > 0)
            Animacion.SetInteger("Dirección", 1);//Adelante
        else
        {
            if (Input.GetAxis("Horizontal") > 0)
                Animacion.SetInteger("Dirección", 2);//Derecha
            else
            {
                if (Input.GetAxis("Horizontal") < 0)
                    Animacion.SetInteger("Dirección", 3);//Izquierda
                else
                {
                    if (Input.GetAxis("Vertical") < 0)
                        Animacion.SetInteger("Dirección", 4);//Atras
                    else
                        Animacion.SetInteger("Dirección", 0);//Reposo
                }
            }
        }

        Animacion.SetBool("Correr", Input.GetButton("Correr"));
        Animacion.SetBool("Saltar", Input.GetButtonDown("Jump"));

        //Ataque Debil (Z)
        Animacion.SetBool("Ataque (Debil)", Input.GetButtonDown("Ataque Debil"));

        //Ataque Fuerte (X)
        Animacion.SetBool("Ataque (Fuerte)", Input.GetButtonDown("Ataque Fuerte"));

        //Defensa (C)
        Animacion.SetBool("Defensa", Input.GetButton("Defensa"));

        //Modo de Asalto
        Animacion.SetBool("Modo De Asalto", ModoDeAsalto);

        if (GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Reposo"))
            aburrido += Time.deltaTime;
        else
        {
            Animacion.SetInteger("Aburrido", 0);
            aburrido = 0;
        }

        if (!GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Aburrido"))
            unavez = false;

        if (aburrido > 10)
        {
            Animacion.SetInteger("Aburrido", Random.Range(1, 5));
            aburrido = 0;
        }

        Animacion.SetInteger("Arma", arma);
        Animacion.SetInteger("Vida", (int)Vida);
        Animacion.SetFloat("delay", GetComponent<AnimationStateInfo>().GetRecredTime());

        if (controller.isGrounded)
            Animacion.SetBool("Aterrizado", true);
        else
            Animacion.SetBool("Aterrizado", false);

        if (GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("En el Aire"))
            Animacion.SetFloat("Tiempo Suspendido", Animacion.GetFloat("Tiempo Suspendido") + Time.deltaTime);
        else
            Animacion.SetFloat("Tiempo Suspendido", 0);

        if (GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Impacto") || solapa > 1)
            Daño = ReciveDaño.Ninguno;

        if (Daño != ReciveDaño.Ninguno)
            solapa += Time.deltaTime;
        else
            solapa = 0;


    }

    private void Rotacion()
    {

        if (GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Caminar") || 
            GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Correr"))
            transform.Rotate(0, Input.GetAxis("Horizontal") * 1.5f, 0);

    }

    private void ControlCamara()
    {
        //La camara se acerca al jugador
        if (GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Correr"))
            Camera.main.transform.parent.position = Vector3.Slerp(Camera.main.transform.parent.position, transform.position, Time.deltaTime * 2);
        else
            Camera.main.transform.parent.position = Vector3.Slerp(Camera.main.transform.parent.position, transform.position, Time.deltaTime);


        //Gira a ver al jugador
        Quaternion targetRotation = Quaternion.LookRotation((transform.position + new Vector3(0, 1.5f, 0)) - Camera.main.transform.position);
        Camera.main.transform.rotation = Quaternion.Slerp(Camera.main.transform.rotation, targetRotation, 3 * Time.deltaTime);

        if (Input.GetKey(KeyCode.I))
        {
            Camera.main.transform.parent.transform.Rotate(Vector3.up);
        }

        if (Input.GetKey(KeyCode.O))
        {
            Camera.main.transform.parent.transform.Rotate(Vector3.down);
        }

        if (Input.GetKey(KeyCode.L))
        {
            if (Camera.main.transform.position.y < transform.position.y + 5.5f)
                Camera.main.transform.parent.transform.Rotate(Vector3.right);
        }

        if (Input.GetKey(KeyCode.K))
        {
            if (Camera.main.transform.position.y > transform.position.y)
                Camera.main.transform.parent.transform.Rotate(Vector3.left);
        }

    }

    private void ActivarModoDeAsalto()
    {
        //Activar modo de Asalto
        if (GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Desenvainar") && !GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Guardar"))
        {
            if (Input.GetButton("Modo de Asalto") && controller.isGrounded)
            {
                if (unavezModoDeAsalto == false)
                {
                    if (ModoDeAsalto)
                        ModoDeAsalto = false;
                    else
                        ModoDeAsalto = true;

                    unavezModoDeAsalto = true;
                }
            }

            if (!Input.GetButton("Modo de Asalto"))
                unavezModoDeAsalto = false;
        }

    }

    private void EnvainarArmas()
    {
        if (GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Desequipar B"))
        {
        }
    }

    private void DesenvainarArmas()
    {
        if (GetComponent<AnimationStateInfo>().GetAnimationStatus().Contains("Equipar B"))
        {
        }
    }

    private GameObject BuscarEnemigos(float distancemin)
    {
        GameObject[] gameobjects;
        gameobjects = GameObject.FindGameObjectsWithTag("Enemigo");
        GameObject closest = null;
        float distance = distancemin;

        foreach (GameObject gameobject in gameobjects)
        {
            float diff = Vector3.Distance(gameobject.transform.position, transform.position);
            if (diff < distance)
            {
                closest = gameobject;
                distance = diff;
            }
        }

        if (closest != null)
            Debug.Log("Enemigo Encontrado: " + closest.name);
        else
            Debug.Log("No hay enemigos cerca");

        return closest;
    }

    private Direcciones OrigenDelGolpe(Transform Objetivo)
    {
        Vector3 Dirrecion = transform.TransformDirection(new Vector3(Objetivo.position.x, 0, Objetivo.position.z) - new Vector3(transform.position.x, 0, transform.position.z));
        Direcciones Respuesta = Direcciones.Ninguna;

        //Esta al frente
        if (Vector3.Angle(Dirrecion, Vector3.back) < 5.0f)
            Respuesta = Direcciones.Adelante;

        //Esta a la derecha
        if (Vector3.Angle(Dirrecion, Vector3.right) < 5.0f)
            Respuesta = Direcciones.Derecha;

        //Esta a la izquierda
        if (Vector3.Angle(Dirrecion, Vector3.left) < 5.0f)
            Respuesta = Direcciones.Izquierda;

        //Esta atras
        if (Vector3.Angle(Dirrecion, Vector3.forward) < 5.0f)
            Respuesta = Direcciones.Atras;

        return Respuesta;
    }



}