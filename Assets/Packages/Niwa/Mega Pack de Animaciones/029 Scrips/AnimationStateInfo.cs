﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStateInfo : MonoBehaviour
{
    private Animator animacion;
    public string animacionencurso;
    private string anim;
    public float tiempodeanimacion;
    private float s = 0;

    void Update()
    {
        if (anim != animacionencurso)
        {
            anim = animacionencurso;
            tiempodeanimacion = s;
            s = 0;
        }

        GetAnimationStatus();
        s += Time.deltaTime;
    }

    public float GetRecredTime()
    {
        return s;
    }

    public string GetAnimationStatus()
    {
        animacion = GetComponentInChildren<Animator>();
        AnimatorClipInfo[] informacion;
        informacion = animacion.GetCurrentAnimatorClipInfo(0);

        if (informacion.Length > 0)
        {
            animacionencurso = informacion[0].clip.name;
        }
        else
            animacionencurso = "Ninguna";

        return animacionencurso;
    }
}
