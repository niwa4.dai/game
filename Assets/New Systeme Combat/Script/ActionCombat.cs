using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCombat
{
    public Fighter lanceur;
    public Fighter cible;
    public Skill skill;

    public ActionCombat(Fighter lanceur, Fighter cible, Skill skill){
        this.lanceur = lanceur;
        this.cible = cible;
        this.skill = skill;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LookTarget(){

    }

    public bool DistanceIsGood(){
        float dis = Vector3.Distance(lanceur.transform.position, cible.transform.position);
        if(dis < skill.range)
            return true;
        return false;    
    }

    public void Execute(){
        if(skill.type == SkillType.Heal)
            cible.Heal(skill.puissance);
        else
            cible.TakeDamage(skill.puissance);

        if(cible.hpCurrent <= 0)
            cible.Death();
        CrystalsManager.tryToAddCrystal(lanceur);
    }

  
}
