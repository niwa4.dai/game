using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionModel : MonoBehaviour
{
    public ActionCombat actionCombat;
    // Start is called before the first frame update
    void Start()
    {
        actionCombat.lanceur.transform.LookAt(actionCombat.cible.transform);

        if(actionCombat.skill.type == SkillType.Default && actionCombat.DistanceIsGood()){
            actionCombat.Execute();
        }
        if(actionCombat.skill.type == SkillType.Heal){
            actionCombat.Execute();
        }

        Destroy(gameObject,actionCombat.skill.timer);
    }

    // Update is called once per frame
    void Update()
    {
    }   

    void OnTriggerEnter(Collider other) {
        if(actionCombat.skill.type == SkillType.Projectil && actionCombat.cible.gameObject == other.gameObject){
            actionCombat.Execute();
        }
        

    }

}
