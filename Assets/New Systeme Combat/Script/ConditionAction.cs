using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionAction : MonoBehaviour
{
    public bool canLancer;
    private FighterAi fighter;
    private Fighter cible;
    private void Start() {
        fighter = GetComponent<FighterAi>();
    }

    public void SetCible(string cible){
        if(cible == Cible.Moi.ToString())
            this.cible = fighter;
        if(cible == Cible.EnnemyLePlusProche.ToString()){
            this.cible = FightManger.Instance.FindNearestOpponents(fighter);
            this.fighter.target = this.cible;
        }
    }

    public void SiPvInferieurA(int pourcent){
        canLancer = false;
        if(fighter.hpCurrent < pourcent){
            canLancer = true;
        }
    }

    public void SiDistanceAvecLaCibleEtInferieurA(float distance){
        canLancer = false;
        float dis = this.cible != null ? Vector3.Distance(transform.position, this.cible.transform.position) : 0f;
        if(dis > distance)
            canLancer = true;
    }

    public void Toujour(){
        canLancer = true;
    }

    public void SeRapprocher(){
        if(canLancer){
            fighter.toFollow = cible.gameObject;
            fighter.Avance();
            fighter.lanceAction = true;
        }else
            fighter.lanceAction = false;
    }

    public void lancer(Skill skill){
        if(canLancer){
            Debug.Log(fighter+" lance "+skill+" sur "+cible);
            fighter.actionCombat = new ActionCombat(fighter,cible, skill);
            fighter.animator.SetFloat("ActionIndex",fighter.actionCombat.skill.animationIndex);
            fighter.animator.SetTrigger("Action");
            fighter.lanceAction = true;
        }else
            fighter.lanceAction = false;
        
    }
}
