using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrystalsImageManager : Singleton<CrystalsImageManager>
{
    public List<Sprite> sprites;
    public Sprite eau;
    public Sprite feu;
    public Sprite vent;
    public Sprite terre;
    public Sprite eauX;
    public Sprite feuX;
    public Sprite ventX;
    public Sprite terreX;

    public Text text;
 
}
