using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeplacer : MonoBehaviour
{
 
     void Update()
    {      
        if(Input.GetMouseButtonDown(1) && !FightManger.Instance.currentPlayer.isDead){
            RaycastHit hit;
            Ray ray = FightManger.Instance.camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit)) {
                transform.position = hit.point;
                transform.Translate(Vector3.up*0.1f, Space.World);
                FightManger.Instance.currentPlayer.toFollow = gameObject;
                FightManger.Instance.currentPlayer.Avance();
                FightManger.Instance.currentPlayer.auto = false;

            }
        }       
    }
}
