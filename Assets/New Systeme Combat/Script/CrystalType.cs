using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CrystalType
{
    Default,Feu,Eau,Terre,Vent,FeuX,EauX,TerreX,VentX
}
