using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;
using System.ComponentModel;

public class Fighter : MonoBehaviour
{   
    public bool enCombat;
    public bool auto;
    public FighterType fighterType;
    public Charactere charactere;
    public FightManger fightManger { get;  set; }
    public float count;
	[HideInInspector]
    public Fighter target;
    public int hpCurrent;
    protected GameObject line;
    public Animator animator;
    public ActionCombat actionCombat;
    public List<Transform> locations;
    public List<CrystalType> crystals;
    private NavMeshAgent navAgent;
    public bool isDead ;
    public PortraitUi portraitUi;


    public GameObject toFollow;
    private void Start() {
        hpCurrent = charactere.hpMax;
        count = charactere.vitesse;
        animator = GetComponent<Animator>();
        navAgent = GetComponent<NavMeshAgent>();

    }

    private void Update() {
        if(isDead)
            return;

        if(!navAgent.isStopped && toFollow != null){
            float dis = Vector3.Distance(transform.position, toFollow.transform.position);
            if(dis < 2f){
                animator.SetFloat("vertical",0f);
                navAgent.isStopped = true;
                navAgent.ResetPath();

            }          
        }
        if(!enCombat || animator.GetFloat("vertical") > 0.1f)
            return;

        if(auto)
            ActionAuto();
        else
            ActionManuel();
    }

    //C'est pour un test cette fonction
    public void ActionManuel(){
        count -= Time.deltaTime;
        if(count < 0){
            if(Input.GetKeyDown("k")){
                LanceAction(charactere.basicSkill);
                count = charactere.vitesse;
            }
            if(Input.GetKeyDown("m")){
                LanceAction(charactere.comptences[0]);
                count = charactere.vitesse;
            }
        }
    }

    public void ActionAuto(){
        count -= Time.deltaTime;
        if(count < 0){
            count = charactere.vitesse;
            LanceAction(charactere.basicSkill);
        }
    }

    public virtual void LanceAction(Skill s){
        target = FightManger.Instance.FindNearestOpponents(this);
        //drawLine();
        actionCombat = new ActionCombat(this,target, s);
        actionCombat.LookTarget();
        if(!auto || actionCombat.DistanceIsGood()){
            animator.SetFloat("ActionIndex",actionCombat.skill.animationIndex);
            animator.SetTrigger("Action"); 
        }
    }

    public void drawLine(){
        if(line != null)
            Destroy(line);
        GameObject newLine = FightManger.Instance.line;
        newLine.GetComponent<DrawLineRender>().Point1 = transform;
        newLine.GetComponent<DrawLineRender>().Point3 = target.transform;
        line = Instantiate(newLine);
        line.transform.SetParent(transform);
    }

    public void InstantiateSkill(){
        int i = (int) actionCombat.skill.location;
        ActionModel model = Instantiate(actionCombat.skill.model);
        if(actionCombat.skill.locationOnCible)
            model.transform.position = actionCombat.cible.locations[i].position;
        else
            model.transform.position = actionCombat.lanceur.locations[i].position;

        if(!actionCombat.skill.locationOnCible)
            model.transform.LookAt(actionCombat.cible.locations[0]);
            
        model.actionCombat = actionCombat;
    }

    public void Death(){
        animator.SetTrigger("death");
        isDead = true;
        if(fighterType != FighterType.Player){
            FightManger.Instance.opponents.Remove(this);
            Destroy(gameObject);
        }
    }

    public void Avance(){
        navAgent.isStopped = false;
        navAgent.SetDestination(toFollow.transform.position);
        animator.SetFloat("vertical",1f);
    }

    public void TakeDamage(int val ){
        hpCurrent -= val;
    } 
    public void  Heal(int val)
    {
        hpCurrent += val;
    } 


    
    public void UseCrystal(CrystalType c){
        if(charactere.comptences[(int)c - 1] != null)
            LanceAction(charactere.comptences[(int)c - 1]);
        CrystalsManager.removeCrystal(this,c);
    }
}