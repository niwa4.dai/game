using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FighterAi : Fighter
{
    public List<UnityEvent> myUnityEvent;
    public bool lanceAction;
    void Awake() {
        StartCoroutine(BougeAleatoirement());
        target = FightManger.Instance.FindNearestOpponents(this);

    }
    void FixedUpdate()
    {
        AnimatorClipInfo[]  m_CurrentClipInfo = animator.GetCurrentAnimatorClipInfo(0);
        string m_ClipName = m_CurrentClipInfo[0].clip.name;
        if(m_ClipName.Contains("Idle"))
            BougeAleatoirement();

        if(target != null){
            transform.LookAt(target.transform);
        }
    }

    int i = 0;
    public override void LanceAction(Skill s) {
        foreach(UnityEvent ue in myUnityEvent){
            if(lanceAction){
                lanceAction = false;
                break;
            }
            ue.Invoke();
        }
        return;
        if(target == null){
            return;
        }
        float dis = target != null ? Vector3.Distance(transform.position, target.transform.position) : 0f;
        if(dis > 3f ){
            SeRaprocher();
        }else{
            if(i == 6){
                Projectil();
                i = 0;
                return;
            }
            Mele();
            i++;
        }
        return;
    }
    void Mele(){
        target = FightManger.Instance.FindNearestOpponents(this);
        Skill skill = charactere.comptences[0];
        actionCombat = new ActionCombat(this,target, skill);
        animator.SetFloat("ActionIndex",actionCombat.skill.animationIndex);
        animator.SetTrigger("Action"); 
    }
    void Soin(){

        Skill skill = charactere.comptences[2];
        actionCombat = new ActionCombat(this,this, skill);
               animator.SetFloat("ActionIndex",actionCombat.skill.animationIndex);
       animator.SetTrigger("Action"); 
       
    }

    void Projectil(){
        target = FightManger.Instance.FindFarestOpponents(this);
        Skill skill = charactere.comptences[1];
        actionCombat = new ActionCombat(this,target, skill);
        animator.SetFloat("ActionIndex",actionCombat.skill.animationIndex);
       animator.SetTrigger("Action"); 
    }

    

    void SeRaprocher(){
        target = FightManger.Instance.FindNearestOpponents(this);
        toFollow = target.gameObject;
        Avance();
    }



    public void Bouge(Mouvement mouvement){
        animator.SetFloat("DeCoter", (float) mouvement);
        animator.SetTrigger("BougeDeCoter");
    }

   IEnumerator BougeAleatoirement(){
        while(true){
            float rdmTime =  UnityEngine.Random.Range(1f,3f);
            yield return new WaitForSeconds(rdmTime);
            AnimatorClipInfo[]  m_CurrentClipInfo = animator.GetCurrentAnimatorClipInfo(0);
            string m_ClipName =  m_CurrentClipInfo[0].clip.name;
            if(m_ClipName.Contains("Idle")){
                float dis = target != null ? Vector3.Distance(transform.position, target.transform.position) : 0f;
                if(dis < 1f && dis > 0f){
                    Bouge(Mouvement.Recule);
                }else
                if(dis > 5f){
                    Bouge(Mouvement.Avance);
                }else{
                    int rdm =  UnityEngine.Random.Range(0,100);
                    if(rdm >= 0 && rdm <= 50)
                        Bouge(Mouvement.Droite);
                    if( rdm > 50)   
                        Bouge(Mouvement.Gauche);
                }
                    
            }
        }        
    } 


 

}
