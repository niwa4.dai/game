using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Skill", menuName = "Personnage/Skill")]
public class Skill : ScriptableObject
{
    public ActionModel model;
    public int puissance;
    public int range;
    public float animationIndex = 0f;
    public float timer = 3f;
    public Location location;
    public bool locationOnCible;
    public SkillType type;


}
