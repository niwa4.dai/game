using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CrystalsManager
{

    public static void tryToAddCrystal(Fighter f){
        
        int rdm =  UnityEngine.Random.Range(0,100);
        if(f.charactere.chanceAddCrystal > rdm){
            addCrystal(f);
        }

    }

    public static void addCrystal(Fighter f){
        if(f.crystals.Count >= 8)
            return;
            
        int rdm =  UnityEngine.Random.Range(0,100);
        CrystalType newC = getRamdomCrystal();
        f.crystals.Add(newC);
        if(possede3SameCristaux(f,newC)){
           fusion(f,newC);
        }
        f.portraitUi.AfficheCrystals();
    }

    public static void removeCrystal(Fighter f, CrystalType c){
        f.crystals.Remove(c);
    }
    
    private static bool possede3SameCristaux(Fighter f,CrystalType c){
        int count = 0;
        foreach (CrystalType item in f.crystals)
        {
            if(item == c){
                count++;
            }
        }
        if(count == 3)
            return true;
        else 
            return false;
    }

    private static void fusion(Fighter f, CrystalType c){
        int count = 0;
        List<CrystalType> l = new List<CrystalType>();
        foreach (CrystalType item in f.crystals)
        {
            if(item == c && count != 3){
                count++;
                if(count == 3){
                    int currentCrital = (int) c;
                    CrystalType newCristal = (CrystalType) currentCrital + 4;
                    l.Add(newCristal);          
                }
            }else{
                l.Add(item);
            }
        }
        f.crystals = l;
    }

    private static CrystalType getRamdomCrystal(){
        int rdm =  UnityEngine.Random.Range(0,100);
        if(rdm >= 0 && rdm <= 25)
            return CrystalType.Eau;
        if(rdm > 25 && rdm <= 50)
            return CrystalType.Terre;
        if(rdm > 50 && rdm <=75)
            return CrystalType.Feu;
        return CrystalType.Vent;
    }


}
