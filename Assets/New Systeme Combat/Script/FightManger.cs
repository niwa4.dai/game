using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightManger : Singleton<FightManger>
{
    public List<Fighter> players;
    public List<Fighter> opponents;
    public GameObject line;
    public Camera camera;
    public Fighter currentPlayer;

    public List<PortraitUi> portraitUis;

    protected void Awake() {
         base.Awake();
        currentPlayer = players[0];
        for (int i = 0; i < players.Count; i++)
        {
            portraitUis[i].fighter = players[i];
            players[i].portraitUi = portraitUis[i];
            portraitUis[i].UpdateUI();
        }
    }

    private void Update() {
       if(Input.GetKeyDown("t"))
            NextPlayer();
        if(Input.GetMouseButtonDown(0)){
           // currentPlayer.auto = true;
        }
        if(Input.GetKeyDown("h"))
            CrystalsManager.addCrystal(currentPlayer);
        if(Input.GetKeyDown("g"))
            currentPlayer.UseCrystal(currentPlayer.crystals[0]);  



    }

    public Fighter FindNearestOpponents(Fighter fighter){
        float minDis = Mathf.Infinity;
        Fighter target = null;
        foreach (Fighter item in GetListOpponents(fighter.fighterType))
        {
            float dis = (fighter.transform.position - item.transform.position).sqrMagnitude;
            if(minDis > dis && !item.isDead){
                target = item;
                minDis = dis;
            }
        }
        return target;
    }

    public Fighter FindFarestOpponents(Fighter fighter){
        float maxDis = -1f;
        Fighter target = null;
        foreach (Fighter item in GetListOpponents(fighter.fighterType))
        {
            float dis = (fighter.transform.position - item.transform.position).sqrMagnitude;
            if(maxDis < dis && !item.isDead){
                target = item;
                maxDis = dis;
            }
        }
        return target;
    }

    public void NextPlayer(){
        int i = players.IndexOf(currentPlayer);
        i++;
        if(i >= players.Count)
            i = 0;
        currentPlayer = players[i];
    }

    public List<Fighter> GetListOpponents(FighterType type){
        switch (type)
        {
            case FighterType.Player :
                return opponents;
            break;
            default:
            break;
        }
       return players;

    }
}
