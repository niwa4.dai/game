using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Hero", menuName = "Personnage/Charactere")]
public class Charactere : ScriptableObject
{
    public Sprite portrait;
    public int hpMax;
    public int vitesse;
    public Skill basicSkill;
    public List<Skill> comptences;
    public int chanceAddCrystal;

    public Vector3 position;
    public Quaternion rotation;

}
