using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortraitUi : MonoBehaviour
{   
    public Image img;
    public Fighter fighter;
    public List<Image> crystals;
    // Start is called before the first frame update
    void Awake()
    {
        img = GetComponent<Image>();
    }

    void Start(){

    }

    // Update is called once per frame
    void Update()
    {
        GenereAideCompetence();
    }

    public void UpdateUI(){
         GetComponent<Image>().sprite = fighter.charactere.portrait;
    }

    public void AfficheCrystals(){
        for (int i = 0; i < crystals.Count; i++)
        {
            if(i < fighter.crystals.Count ){
                crystals[i].sprite = CrystalsImageManager.Instance.sprites[(int) fighter.crystals[i] - 1];
                crystals[i].gameObject.SetActive(true);

            }
            else
                crystals[i].gameObject.SetActive(false);
        }
    }


    public void SelectPlayer(){
        FightManger.Instance.currentPlayer = fighter;
        CrystalsImageManager.Instance.text.text = GenereAideCompetence();
    }

    public void UseCrystal(int i){
        fighter.UseCrystal(fighter.crystals[i]);
        AfficheCrystals();
    }


    public string GenereAideCompetence(){
        string res = "";

        res += GenereStringCristal("Feu",0);
        res += GenereStringCristal("Eau",1);
        res += GenereStringCristal("Terre",2);
        res += GenereStringCristal("Vent",3);
        res += "\n";
        res += GenereStringCristal("FeuX",4);
        res += GenereStringCristal("EauX",5);
        res += GenereStringCristal("TerreX",6);
        res += GenereStringCristal("VentX",7);
        Debug.Log(res);
        return res;
    }

    private string GenereStringCristal(string c, int i){
        if(fighter.charactere.comptences[i] == null)
            return "";
        return "Le crystal "+c+" est associé à "+fighter.charactere.comptences[i].name+"\n";


    }
}
