using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FighterType
{
    Player, Adversaire, Familier
}
