﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PersoCurrentCompData", menuName = "Competence/PersoCurrentComp", order = 1)]

public class PersoCurrentCompData : ScriptableObject
{
    public CompetenceData eau;
    public CompetenceData feu;
    public CompetenceData terre;
    public CompetenceData vent;
    public CompetenceData eauX;
    public CompetenceData feuX;
    public CompetenceData terreX;
    public CompetenceData ventX;
    public CompetenceData defaut;

}
