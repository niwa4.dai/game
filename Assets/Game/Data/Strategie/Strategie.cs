﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "Strategie", menuName = "Personnage/Strategie")]
public class Strategie : ScriptableObject {

    public string description;
   
    public Color color;

    public Sprite logo;

}
