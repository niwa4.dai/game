﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "Objet", menuName = "Personnage/Objet")]
public class ObjetData : ScriptableObject {

    public string description;
    public Sprite logo;
    public Strategie strategie;

    public GameObject effet;
}
