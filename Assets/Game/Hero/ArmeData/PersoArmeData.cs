﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ArmeData", menuName = "Arme/ArmeDataPerso", order = 1)]
public class PersoArmeData : ScriptableObject
{
    public ArmeData[] armes;
}
