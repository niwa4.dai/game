﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ArmePosseder", menuName = "Arme/ArmePosseder", order = 1)]

public class ArmePosseder : ScriptableObject
{
    public ArmeData[] armes;
}
