﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TPCWC;


public class SkeletonMageGambit : Gambit
{

private int index = 0;
    public float a;
    float dis ;
    public int phase = 1;

    public override void preparAction()
    {   
      phase1();
    }

    public void phase1(){
        GameObject cible = FindCible.findHeroLePlusProcheSansObstacle(transform);

        if(cible == cibleCurrent && actionEnAttente != null)
            return;

        cibleCurrent = cible;
       
        dis  = Vector3.Distance(cibleCurrent.transform.position, transform.position);

        if(cibleCurrent == null){
          return ;
        }

       
      attaqueDeLoin();
      actionEnAttente = hitScript.action;   
    }


    private void attaque()
    {
        actionManager.prepareAction("Coup Sans Effet",1f,"Devant",cibleCurrent);
    }

    private void attaqueDeLoin()
    {
        actionManager.prepareAction("Projectile 5",4f,"Devant",cibleCurrent);
    }
    private void seRaproche(){
        companion.chase2(2f, cibleCurrent);
        hitScript.action = null;
    }

    public override MyAction useCrystal(string name)
    {   
        return null;
    }

    public void useCrystalPhase1(string name)
    {   
       GameObject cible = FindCible.findHeroLePlusProcheSansObstacle(transform);
        cibleCurrent = cible;
        if(cibleCurrent == null){
          return ;
        }
        dis  = Vector3.Distance(cibleCurrent.transform.position, transform.position);
        Debug.Log("Apollo utilise "+name);
              //          actionManager.prepareAction("SoinApollo",5f,"Centre",gameObject);

        switch (name)
        {
            case "Feu+":
                actionManager.prepareAction("Projectile 5",4f,"Devant",cibleCurrent);
                break;
            case "Eau+":
                actionManager.prepareAction("SoinApollo",5f,"Centre",gameObject);
                break;
            case "Vent+":
                actionManager.prepareAction("Aqua Vortex", 1f, "", cibleCurrent);
                break;
            case "Terre+":
                actionManager.prepareAction("Coup 3", 0f, "", cibleCurrent);
                break;
            default:
                break;
        }
        
        animator.SetTrigger("Action");
        hitScript.afficheCercleMagic();
        useCristal = true;
        GetComponent<CristalScript>().deleteCrystal(name);
        StartCoroutine(actionManager.useCristal(hitScript.action.competence.timeCanalisation));

        actionEnAttente = hitScript.action;
    }


}
