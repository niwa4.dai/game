using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterBehaviourAi : MonoBehaviour
{
    [SerializeField]
    public List<TestCl> paths;
    private NavMeshAgent navMesh;
    public int currentPoint;
    public int currentPath;
    public bool marche;

    public float waitTimer;
    private void Start() {
        navMesh = GetComponent<NavMeshAgent>();

       // Play(0);
    }


    private void Update() {
        if(!marche)
            return;

        
        float dis = Vector3.Distance(transform.position, paths[currentPath].path[currentPoint].position);
        
        if(dis < 1.5f){
             if(paths[currentPath].attendAChaqueArret > 0f && waitTimer > 0f){
                waitTimer -= Time.deltaTime;
                return;
            }else if(paths[currentPath].attendAChaqueArret > 0f){
                waitTimer = paths[currentPath].attendAChaqueArret;
            }
            currentPoint++;
           

           if(paths[currentPath].path.Count == currentPoint && !paths[currentPath].loop ){
                marche = false; 
                navMesh.ResetPath();
                return;
            }else if(paths[currentPath].path.Count == currentPoint && paths[currentPath].loop ){
                currentPoint = 0;
            }

        
            navMesh.SetDestination(paths[currentPath].path[currentPoint].position);
            navMesh.speed = paths[currentPath].speed;

        }
    }

    public void Play(int index){
        currentPath = index;
        currentPoint = 0;
        if(paths[currentPath].path.Count == 0)
            return;
        navMesh.SetDestination(paths[currentPath].path[currentPoint].position);
        navMesh.speed = paths[currentPath].speed;
        navMesh.isStopped = false;
        marche = true;
        waitTimer = paths[currentPath].attendAChaqueArret;
    }
}
[System.Serializable]
public class TestCl
{
    public List<Transform> path;
    public float speed;

    public float attendAChaqueArret;
    public bool loop;
}
