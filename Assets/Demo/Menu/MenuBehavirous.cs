using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBehavirous : Singleton<MenuBehavirous>
{
    public GameData gameData;
    public GameObject menu;
    public List<CharactereMenu> characteresMenu;

    private void Update() {
        if(Input.GetKeyDown("o"))
            OpenMenu();
    }

    public void OpenMenu(){
        Time.timeScale = 0;
        menu.SetActive(true);
        init();
    }


    public void CloseMenu(){
        Time.timeScale = 1;
        menu.SetActive(false);
    }

    private void init(){
        for (int i = 0; i < characteresMenu.Count; i++)
        {
            if(gameData.characteres.Count > i){
                characteresMenu[i].charactere = gameData.characteres[i];
                Debug.Log(""+gameData.characteres[i]);
                characteresMenu[i].init();
            }else{
                characteresMenu[i].gameObject.SetActive(false);
            }   
        }
    }
}
